Lumen
================
## Como instalar el proyecto

Antes de continuar, tener instalado lo siguiente:

* MongoDB <http://www.mongodb.org/downloads/>
* Ruby <https://www.ruby-lang.org/en/downloads/>
    * compass (gem install compass) <http://compass-style.org/install/>
* grunt-cli (npm install -g grunt-cli) <http://gruntjs.com/getting-started>

### Setup
Ejecutar `npm install`, followed by `bower install` para instalar las dependencias.

### Lanzar la aplicación
Ejecutar `grunt server` para ejecutar la aplicación en modo desarrollo con livereload, o ejecutar `grunt server:dist` para ejecutarla optimizada para producción.

### Testing
Ejecutar `grunt test` para comenzar el karma test runner.

### Coding Standard
https://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml#Naming

## Directory structure
    +---server.js           -> Bootstrap Server
    |
    +---app                 -> Client
    |   +---scripts
    |   |   +---controllers
    |   |   +---directives
    |   |   \---services
    |   |
    |   +---styles
    |   \---views
    |       \---partials
    |           \---professions
    +---lib                 -> Server
    |   +---config
    |   +---controllers
    |   +---db
    |   \---models
    |           
    +---test                -> Client unit tests
