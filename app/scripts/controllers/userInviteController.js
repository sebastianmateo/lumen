'use strict';

angular.module('angularPassportApp')
  .controller('UserInviteController', ['$scope', 'User', 'Invite', 'Groups', 'Inns',
    function ($scope, User, Invite, Groups, Inns) {
      $scope.inviteData = {};

      Groups.query(function( groups){
        $scope.groups = groups;
        $scope.inviteData.email = "";
        $scope.inviteData.group = groups[0];
      });

      Inns.query(function(inns) {
        $scope.inns = inns;
      });

      Invite.query(function (invites) {
        $scope.pendingInvites = invites;
      });

      $scope.invite = function() {
        $scope.error = "";
        var invite = new Invite({
          email: $scope.inviteData.email,
          group: $scope.inviteData.group._id
        });
        invite.$save({},
          function (response) {
            console.log(response);
            Invite.query(function (invites) {
              $scope.pendingInvites = invites;
            });
          },
          function (error) {
            $scope.error = "La dirección de correo electrónico ya existe";
          }
        );
      };
    }]);
