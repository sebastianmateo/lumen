'use strict';

angular.module('angularPassportApp')
  .controller('NavbarCtrl', ['$scope', 'Auth', 'Events', 'Messages', '$location', '$window', '$interval', function ($scope, Auth, Events, Messages, $location, $window, $interval) {

    var unread = function() {
      var count = 0;
      for (var key = 0; key < this.length; key++)
      {
        if (!this[key].read) {
          count += 1;
        }
      }

      return count;
    };

    var process_events = function(events) {
      $scope.events = events;
      $scope.events.unread = unread;
    };

    var process_messages = function() {
      $scope.unread_messages = Messages.newMessagesCount();
    };

    var adminMenu = {
      "general": [
        {
          "title": "Profesiones",
          "link": "professions"
        },
        {
          "title": "Áreas Temáticas",
          "link": "thematic"
        },
        {
          "title": "Poblaciones",
          "link": "populations"
        },
        {
          "title": "Géneros",
          "link": "gender"
        },
        {
          "title": "Posadas",
          "link": "inns"
        }
      ],
      "tabs": [
        {
          "title": "Usuarios",
          "options": [
            {
              "title": "Listado",
              "link": "users"
            },
            {
              "title": "Invitar",
              "link": "users.invite"
            }
          ]
        }
      ]
    };

    var posaderoMenu = {
      "general": [
        {
          "title": "Perfil",
          "link": "profile"
        },
        {
          "title": "Alta de Asistido",
          "link": "aided.add"
        },
        {
          "title": "Ver Asistidos",
          "link": "aided"
        }
      ],
      "tabs": []
    };

    var voluntarioMenu = {
      "general": [
        {
          "title": "Perfil",
          "link": "profile"
        },
        {
          "title": "Derivar Asistido",
          "link": "aided.refer"
        }
      ],
      "tabs": []
    };

    var noauthMenu = {
      "general": [],
      "tabs": []
    };

    var processTabs = function(currentUser){
      if (currentUser){
        switch (currentUser.role){
          case 0:
            $scope.menu = adminMenu;
            break;
          case 1:
            $scope.menu = posaderoMenu;

            //Los eventos tenemos que pedirlos solo como posaderos
            $interval(Events.requestEvents, 1000, 0, true, process_events);

            //Lo mismo los mensajes
            $interval(Messages.requestMessages, 1000);
            process_messages();
            $scope.$on('newMessages', process_messages);

            break;
          case 2:
            $scope.menu = voluntarioMenu;

            //Inicializo los mensajes
            $interval(Messages.requestMessages, 1000);
            process_messages();
            $scope.$on('newMessages', process_messages);

            break;
        }
      } else {
        $scope.menu = noauthMenu;
      }
    };

    //Listen to changed user to update tabs
    $scope.$watch('currentUser', function (currentUser) {
      processTabs(currentUser);
    });

    $scope.logout = function () {
      Auth.logout(function (err) {
        if (!err) {
          $window.location.href = '/';
          $location.path('/login');

        }
      });
    };
  }]);
