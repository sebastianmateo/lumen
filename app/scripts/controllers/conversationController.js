'use strict';

angular.module('angularPassportApp')
  .controller('ConversationController', ['$scope', 'Conversations', 'Messages', 'User', '$location', '$routeParams',
    function ($scope, Conversations, Messages, User, $location, $routeParams) {

      $scope.pictures = {};

      var getProfilePicture = function(id) {
        User.query(function (users) {
          for (var i = 0; i < users.length; i++) {
            var user = users[i];
            if (user._id == id)
              $scope.pictures[id] = user.profilePicture;
          }
        });
      };

      $scope.find = function () {
        Conversations.query(function (conversations) {
          $scope.conversations = conversations;
        });
      };

      $scope.create = function () {
        var conversation = new Conversations({
          subject: $scope.subject,
          content: $scope.content,
          to: $routeParams.userId
        });

        conversation.$save(function (response) {
          $location.path("conversations");
        });
      };

      $scope.findOne = function () {
        var conversationId = $routeParams.conversationId;
        Conversations.get({
          conversationId: conversationId
        }, function (conversation) {
          Messages.requestMessages();
          $scope.conversation = conversation;
          //Cargo los nombres de los usuarios para usar en vez del id
          $scope.names = {};
          for (var i = 0; i < $scope.conversation.participants.length; i++) {
            $scope.names[$scope.conversation.participants[i]._id] = $scope.conversation.participants[i].name + ' ' + $scope.conversation.participants[i].lastName;
            getProfilePicture($scope.conversation.participants[i]._id);
          }
          for (var i = 0; i < $scope.conversation.messages.length; i++){
            $scope.conversation.messages[i].fromName = $scope.names[$scope.conversation.messages[i].from];
          }
        });
      };

      $scope.hasUnreadMessages = function(conversation) {
        return Messages.hasUnreadMessages(conversation);
      };

      $scope.send = function () {
        var conversation = $scope.conversation;
        conversation.$update(function () {
          $scope.findOne();
        });
      };
    }]);
