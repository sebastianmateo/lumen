'use strict';

angular.module('angularPassportApp')
    .controller('InnController', ['$scope', 'Inns', 'Groups', '$location', '$routeParams',  function ($scope, Inns, Groups, $location, $routeParams) {

    $scope.insertingGroup= false;
    $scope.editingGroup = false;

		$scope.create = function () {
      console.log("Creating inn");
			var inn = new Inns({
				name: this.name,
        phone: this.phone,
        address: this.address
			});

			inn.$save(function (response) {
        $scope.inns.push(inn);
        $scope.name = "";
        $scope.phone = "";
        $scope.address = "";
			});
		};

		$scope.remove = function (index) {
      var inn = $scope.inns[index];
      inn.$remove();

      $scope.inns.splice(index, 1);
		};

		$scope.update = function (data, attribute) {
      var inn = $scope.inn;
      inn[attribute] = data;
      inn.$update();
		};

		$scope.find = function () {
      Inns.query(function (inns) {
        $scope.inns = inns;
      });
		};

		$scope.findOne = function () {
			Inns.get({
				innId: $routeParams.innId
			}, function (inn) {
        console.log(inn);
				$scope.inn = inn;
			});
		};

    //Groups
    $scope.saveGroup = function(index, data){
      //Si estoy insertando un nuevo grupo, tengo que crearlo
      console.log("Saving group");
      if ($scope.insertingGroup){
        var group = new Groups({
          name: data.name,
          day: data.day,
          time: data.time,
          inn: $scope.inn._id
        });

        group.$save(function (response) {
          //Remove the temporary group
          $scope.inn.groups.splice(index, 1);
          //Insert the real one
          $scope.inn.groups.push(group);
        });
      } else {
        //Sino actualizo el existente
        var group = new Groups($scope.inn.groups[index]);
        group.name = data.name;
        group.day = data.day;
        group.time = data.time;
        group.$update();
      }

      $scope.insertingGroup = false;
      $scope.editingGroup = false;
    };

    $scope.cancelGroup = function (index, parentIndex) {
      if ($scope.insertingGroup){
        $scope.inn.groups.splice(index, 1);
      }

      $scope.insertingGroup = false;
      $scope.editingGroup = false;
    };

    $scope.removeGroup = function (index) {
      var group = $scope.inn.groups[index];

      var group = new Groups(group);
      group.$remove();

      $scope.inn.groups.splice(index, 1);
    };

    $scope.editGroup = function(){
      $scope.editingGroup = true;
    };

    $scope.addGroup = function (index) {
      $scope.inserted = {};
      $scope.insertingGroup = true;
      $scope.inn.groups.push($scope.inserted);
    };
  }]);
