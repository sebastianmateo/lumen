'use strict';

angular.module('angularPassportApp')
    .controller('TagController', ['$scope', 'Tags', '$location', '$routeParams', 'Sections', function ($scope, Tags, $location, $routeParams, Sections) {
    $scope.sections = Sections.query().sections;
    $scope.insertingSubtag = false;
    $scope.editingSubtag = false;

		$scope.create = function () {
			var tag = new Tags({
				name: this.name,
        category: this.category
			});

			tag.$save(function (response) {
        $scope.tags.push(tag);
        $scope.name = "";
			});
		};

    $scope.init = function(category, subcategory){
      var urls = {
        "THEMATIC_AREA": "thematic",
        "POPULATION" : "populations",
        "GENDER" : "gender",
        "PROFESSION" : "professions"
      };

      $scope.tagUrl = urls[category];
      this.category = category;
      this.subcategory = subcategory;
    };

		$scope.remove = function (index) {
      var tag = $scope.tags[index];
      tag.$remove();

			for (var i in $scope.tags) {
        if ($scope.tags[i] == tag) {
          $scope.tags.splice(i, 1);
        }
      }
		};

		$scope.update = function (data) {
      var tag = $scope.tag;
      tag.name = data;
      tag.$update();
		};

		$scope.find = function () {
      Tags.search({category:this.category}, function (tags) {
        $scope.tags = tags;
      });
		};

		$scope.findOne = function () {
			Tags.get({
				tagId: $routeParams.tagId
			}, function (tag) {
				$scope.tag = tag;
			});
		};

    //Subtags
    $scope.saveSubtag = function(index, data){
      //Si estoy insertando un nuevo subtag, tengo que crearlo
      if ($scope.insertingSubtag){
        var subtag = new Tags({
          name: data.name,
          category: this.subcategory,
          parent: $scope.tag._id
        });

        subtag.$save(function (response) {
          //Remove the temporary subtag
          $scope.tag.subtags.splice(index, 1);
          //Insert the real one
          $scope.tag.subtags.push(subtag);
        });

        $scope.insertingSubtag = false;
      } else {
        //Sino actualizo el existente
        var subtag = $scope.tag.subtags[index];

        var tag = new Tags(subtag);
        tag.name = data.name;
        tag.$update();
      }

      $scope.insertingSubtag = false;
      $scope.editingSubtag = false;
    };

    $scope.cancelSubtag = function (index, parentIndex) {
      if ($scope.insertingSubtag){
        $scope.tag.subtags.splice(index, 1);
      }

      $scope.insertingSubtag = false;
      $scope.editingSubtag = false;
    };

    $scope.removeSubtag = function (index) {
      var subtag = $scope.tag.subtags[index];

      var tag = new Tags(subtag);
      tag.$remove();

      $scope.tag.subtags.splice(index, 1);
    };

    $scope.editSubtag = function(){
      $scope.editingSubtag = true;
    };

    $scope.addSubtag = function (index) {
      $scope.inserted = {};
      $scope.insertingSubtag = true;
      $scope.tag.subtags.push($scope.inserted);
    };

    $scope.$watchCollection('tag.sections', function(newSections, oldSections) {
      if (typeof oldSections != 'undefined') {
        $scope.tag.$update();
      }
    });
  }]);
