'use strict';

angular.module('angularPassportApp')
  .controller('ReferController', ['$scope', 'AidedData', 'Groups', 'Referral', '$location', 'Upload', '$timeout',
    function ($scope, AidedData, Groups, Referral, $location, Upload, $timeout) {
      $scope.groups = Groups.query();
      $scope.aidedData = AidedData.query();
      $scope.referral = {};

      $scope.refer = function (){
        var referral = new Referral({
          name: $scope.referral.name,
          lastName: $scope.referral.lastName,
          dni: $scope.referral.dni,
          location: $scope.referral.location,
          referReasons: $scope.referral.reasons,
          necessities: $scope.referral.necessities,
          group: $scope.referral.group,
          profilePicture: $scope.referral.profilePicture
        });

        referral.$save(function(response){
          $location.path("");
        });
      };

      $scope.uploadFiles = function(file, errFiles) {
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
          file.upload = Upload.upload({
            url: 'api/upload',
            file: file
          });

          file.upload.then(function (response) {
            $timeout(function (){
              $scope.referral.profilePicture = response.data;
            });

          }, function (response) {
            if (response.status > 0)
              $scope.errorMsg = response.status + ': ' + response.data;
          }, function (evt) {
            file.progress = Math.min(100, parseInt(100.0 *
              evt.loaded / evt.total));
          });
        }
      }
    }]);
