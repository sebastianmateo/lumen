angular.module('angularPassportApp')
  .controller('PosaderoReferController', ['$scope', 'User', 'PosaderoReferral', '$uibModalInstance', 'aidedId',
    function ($scope, User, PosaderoReferral, $uibModalInstance, aidedId) {
    $scope.aidedId = aidedId;

    $scope.find = function () {
      User.posaderos(function (users) {
        for (var i = 0; i < users.length; ++i) {
          users[i].search = users[i].name + ' ' + users[i].lastName;
        }
        $scope.users = users;
      });
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };

    $scope.refer = function (posaderoId) {
      console.log($scope.aidedId);

        var referral = new PosaderoReferral({
          referId: $scope.aidedId,
          targetPosaderoId: posaderoId
        });

        referral.$save(function(response){
          $uibModalInstance.dismiss('cancel');
        });
    };
  }]);
