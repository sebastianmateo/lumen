'use strict';

angular.module('angularPassportApp')
  .controller('UserController', ['$scope', 'User', 'Tags', 'Groups', 'Inns', '$location',
    function ($scope, User, Tags, Groups, Inns, $location) {

      $scope.user = {
        professions: [],
        subprofessions: [],
        thematicAreas: [],
        subthematicAreas: [],
        populations: [],
        sexes: []
      };

      Groups.query(function( groups){
        $scope.groups = groups;
      });

      Inns.query(function(inns) {
        $scope.inns = inns;
      });

      Tags.query(function(tags) {
        $scope.tags = tags;
      });

      $scope.find = function () {
        User.query(function (users) {
          for (var i = 0; i < users.length; ++i) {
            users[i].fullName = users[i].name + ' ' + users[i].lastName;
          }
          $scope.users = users;
        });
      };

      Array.prototype.diff = function(a) {
        return this.filter(function(i) {return a.indexOf(i) < 0;});
      };

      $scope.$watch('search.role', function(newValue, oldValue)
      {
        if (newValue == 2) {
          $scope.search.group = undefined;
        }
      });

      $scope.$watchCollection('user.professions', function(newTags, oldTags)
      {
        if (typeof oldTags != 'undefined' && $scope.tags)
        {
          var id = oldTags.diff(newTags);
          for(var i = 0, len = $scope.tags.length; i < len; i++)
          {
            if ($scope.tags[i]._id == id)
            {
              var tag = $scope.tags[i];
              for (var j = 0, l = tag.subtags.length; j < l; j++)
              {
                var index = $scope.user.subprofessions.indexOf(tag.subtags[j]);
                if (index != -1)
                {
                  $scope.user.subprofessions.splice(index);
                }
              }
              break;
            }
          }
        }
      });

      $scope.$watchCollection('user.thematicAreas', function(newTags, oldTags)
      {
        if (typeof oldTags != 'undefined' && $scope.tags)
        {
          var id = oldTags.diff(newTags);
          for(var i = 0, len = $scope.tags.length; i < len; i++)
          {
            if ($scope.tags[i]._id == id)
            {
              var tag = $scope.tags[i];
              for (var j = 0, l = tag.subtags.length; j < l; j++)
              {
                var index = $scope.user.subthematicAreas.indexOf(tag.subtags[j]);
                if (index != -1)
                {
                  $scope.user.subthematicAreas.splice(index);
                }
              }
              break;
            }
          }
        }
      });

      $scope.message = function (e, user) {
        e.preventDefault();
        $location.path("conversations.new/" + user._id);
      };

      $scope.groupFilter = function(group)
      {
        if ($scope.search && $scope.search.group && $scope.search.group.inn && group && group.inn)
          return (group.inn.name == $scope.search.group.inn.name);

        return false;
      };

      $scope.tagFilter = function(user)
      {
        var matches = true;
        //console.log(user.tags);
        for (var key in $scope.user)
        {
          for (var i = 0; i < $scope.user[key].length; i++)
          {
            var found = false;
            for (var _key in user.tags)
            {
              var tag = user.tags[_key];
              if (tag._id == $scope.user[key][i])
                found = true;
            }

            if (!found)
            {
              //console.log("ID not found in key: " + key);
              matches = false;
            }
          }
        }

        return matches;
      };
    }]);
