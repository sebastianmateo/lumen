'use strict';

angular.module('angularPassportApp')
  .controller('CreateAidedController', ['$scope', 'Aided', '$location',
    function ($scope, Aided, $location) {

      $scope.create = function () {
        var aided = new Aided({
          name: this.name,
          lastName: this.lastName,
          dni: this.dni
        });
        aided.$save(function (response) {
          $location.path("aided/" + response.aidedId);
        });
      };
    }]);
