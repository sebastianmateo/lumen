'use strict';

angular.module('angularPassportApp')
    .controller('ProfileController', ['$scope', 'User', 'Tags', 'Groups', '$routeParams', '$location', '$rootScope', 'Upload', 'inArrayFilter', '$timeout',
	function ($scope, User, Tags, Groups, $routeParams, $location, $rootScope, Upload, InArrayFilter, $timeout) {
    $scope.tags = Tags.query();
    $scope.groups = Groups.query();
    $scope.user = {};

    User.get(function (user) {
        $scope.user = user;
    });

    Array.prototype.diff = function(a) {
      return this.filter(function(i) {return a.indexOf(i) < 0;});
    };

    $scope.$watchCollection('user.tags', function(newTags, oldTags) {
      if (typeof oldTags != 'undefined'){
        //Obtengo el tag que se cambio y lo busco en los tags
        var id = oldTags.diff(newTags);
        for(var i = 0, len = $scope.tags.length; i < len; i++) {
          if ($scope.tags[i]._id == id){
            //Cuando encuentro el tag, recorro los hijos
            var tag = $scope.tags[i];
            for (var j = 0, l = tag.subtags.length; j < l; j++){
              //Si alguno de los hijos está agregado a los tags del usuario, lo eliminó de ahí
              var index = $scope.user.tags.indexOf(tag.subtags[j]);
              if (index != -1){
                $scope.user.tags.splice(index);
              }
            }
            break;
          }
        }
      }
    });

    $scope.update = function (form) {
        $scope.user.$update(function (user) {
            $scope.user = user;
        });
    };

    $scope.find = function () {
       User.get({
            userId: $routeParams.userId
        }, function (user) {
            $scope.user = user;
        });
    };

    $scope.findOne = function () {
        User.get({
            userId: $scope.currentUser._id
        }, function (user) {
            $scope.user = user;
        });
    };

    $scope.uploadFiles = function(file, errFiles) {
      $scope.f = file;
      $scope.errFile = errFiles && errFiles[0];
      if (file) {
        file.upload = Upload.upload({
          url: 'api/userPicture',
          data: {name: $scope.currentUser._id, file: file}
        });

        file.upload.then(function (response) {
          $timeout(function (){
            $scope.user.profilePicture = response.data;
          });

        }, function (response) {
          if (response.status > 0)
            $scope.errorMsg = response.status + ': ' + response.data;
        }, function (evt) {
          file.progress = Math.min(100, parseInt(100.0 *
            evt.loaded / evt.total));
        });
      }
    };

    }]);
