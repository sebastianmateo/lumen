'use strict';

angular.module('angularPassportApp')
  .controller('EventController', ['$scope', 'Events', function ($scope, Events) {

    console.log("Events");
    //$scope.events = Events.getEvents();
    //Events.markEventsAsRead();
    Events.requestEvents(function (events)
    {
      console.log("Received events");
      $scope.events = events;
      Events.markEventsAsRead();
    });
  }]);
