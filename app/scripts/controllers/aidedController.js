'use strict';

angular.module('angularPassportApp')
  .controller('AidedController', ['$scope', 'Aided', 'AidedData', 'Inns', 'Referral', '$location', '$routeParams', '$rootScope', 'Upload', '$http', '$timeout', 'User', '$uibModal',
    function ($scope, Aided, AidedData, Inns, Referral, $location, $routeParams, $rootScope, Upload, $http, $timeout, User, $uibModal) {
      $scope.aidedData = AidedData.query();
      $scope.inns = Inns.query();

      $scope.picker = { opened: false };

      $scope.openPosaderoRefer = function (size) {

        var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'partials/aided/aided.posadero.refer.html',
          controller: 'PosaderoReferController',
          size: size,
          resolve: {
            aidedId: function () {
              return $scope.aided._id;
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          $scope.selected = selectedItem;
        }, function () {
          console.log("Dismissed");
        });
      };


      $scope.openPicker = function() {
        $timeout(function() {
          $scope.picker.opened = true;
        });
      };

      $scope.closePicker = function() {
        $scope.picker.opened = false;
      };

      $scope.remove = function (aided) {
        aided.$remove();

        for (var i in $scope.aideds) {
          if ($scope.aideds[i] == aided) {
            $scope.aideds.splice(i, 1);
          }
        }
      };

      $scope.updateCollection = function (section, collection)
      {
        var payload = {};
        payload[section] = {};
        payload[section][collection] = $scope.aided[section][collection];

        if (collection == 'places')
          $scope.addPlacesMarkers();

        $http({
          url: $location.absUrl().replace($location.url(), '') + '/api' + $location.url(),
          method: "PUT",
          data: payload
        })
          .then(function(response) {
            console.log("Success!");
          },
          function(response) {
            console.log("Failed!");
          });
      };

      $scope.showChecklist = function(checklist)
      {
        var selected = [];
        angular.forEach(checklist, function(s) {
          if (checklist.indexOf(s) >= 0) {
            selected.push(s);
          }
        });
        return selected.length ? selected.join(',') : 'Seleccione una o más opciones';
      };

      $scope.update = function (args) {
        var payload = eval(args[0].replace('%data%', "'" + args[1] + "'"));

        $http({
          url: $location.absUrl().replace($location.url(), '') + '/api' + $location.url(),
          method: "PUT",
          data: payload
        });
      };

      $scope.find = function () {
        Aided.query(function (aidedPeople) {
          //Agrego un campo para para filtrar
          for (var i = 0; i < aidedPeople.length; ++i) {
            var temp = aidedPeople[i];
            aidedPeople[i].search = aidedPeople[i].name + ' ' + aidedPeople[i].lastName + ' ' + aidedPeople[i].dni;
          }
          $scope.showMyAided = false;
          $scope.aidedPeople = aidedPeople;
        });
      };

      $scope.showVisiblePlaces = function () {
        for (var i = 0; i < $scope.markers.length; i++) {
          $scope.markers[i].setVisible(true);
          $scope.bounds.extend($scope.markers[i].getPosition());
        }

        $scope.map.fitBounds($scope.bounds);
        google.maps.event.addListenerOnce($scope.map, 'idle', function() {
          google.maps.event.trigger($scope.map, 'resize');
        });
      };

      $scope.addPlacesMarkers = function() {
        if (typeof($scope.aided.personal) != 'undefined'){
          $scope.markers = [];
          $scope.bounds = new google.maps.LatLngBounds();
          for (var i = 0; i < $scope.aided.personal.places.length; i++)
          {
            var place = $scope.aided.personal.places[i];

            if (place.city == '' || place.province == '' || place.street == '' || place.streetNumber == '')
              continue;

            var address = place.street + " " + place.streetNumber + ", " + place.city;

            $scope.geocoder.geocode({'address': address}, function(results, status)
            {
              if (status == google.maps.GeocoderStatus.OK)
              {
                $scope.markers.push(new google.maps.Marker(
                  {
                    map: $scope.map,
                    position: results[0].geometry.location,
                    title: address
                  }));
              }
              
              if ($scope.markers.length == $scope.aided.personal.places.length)
                $scope.showVisiblePlaces();
            });
          }
        }
      };

      $scope.findOne = function () {
        Aided.get({
          aidedId: $routeParams.aidedId
        }, function (aided) {
          $scope.aided = aided;
          $scope.isSubscribed = aided.users.indexOf($scope.currentUser._id) != -1;
          $scope.bounds = new google.maps.LatLngBounds();
          $scope.markers = [];
          $scope.geocoder = new google.maps.Geocoder();
          $scope.map = new google.maps.Map(document.getElementById("map"),
            {
              mapTypeId: google.maps.MapTypeId.ROADMAP
            });

          $scope.addPlacesMarkers();
        });
      };

      $scope.subscribe = function(subscribe){
        if (subscribe){
          User.subscribe({subscribeAidedId: $scope.aided._id});
          $scope.isSubscribed = true;

          //Actualizo la lista de subscripciones local
          $scope.currentUser.aided.push($scope.aided._id);

        } else {
          User.unsubscribe({subscribeAidedId: $scope.aided._id});
          $scope.isSubscribed = false;

          //Actualizo la lista de subscripciones local
          var index = $scope.currentUser.aided.indexOf($scope.aided._id);
          $scope.currentUser.aided.splice(index, 1);
        }
      };

      $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
      };

      $scope.validate = function(data, pattern, errorMessage)
      {
        var re = new RegExp(pattern);
        if (data.match(re) == null)
          return errorMessage;
      }

      $scope.yesNoOptions = [{value: undefined, text:''},{value: false, text: 'No'},{value: true, text: 'Sí'}];
      $scope.genderOptions = [{value: true, text: 'Masculino'}, {value: false, text: 'Femenino'}];
      $scope.jobTypes = [{value: true, text: 'Particular'}, {value: false, text: 'Empresa'}];

      $scope.addToCollection = function(section, collection) {
        var newElement = {};
        $scope.aided[section][collection].push(newElement);
      };

      $scope.addToSubcollection = function(section, collection, subcollection, index) {
        var newElement = {};
        if (typeof $scope.aided[section][collection][index][subcollection] === 'undefined')
          $scope.aided[section][collection][index][subcollection] = [];

        $scope.aided[section][collection][index][subcollection].push(newElement);
      },

        $scope.clearFromCollection = function(section, collection, index) {
          $scope.aided[section][collection].splice(index, 1);
          $scope.updateCollection(section, collection);
        };

      $scope.clearFromSubcollection = function(section, collection, subcollection, parentIndex, index) {
        $scope.aided[section][collection][parentIndex][subcollection].splice(index,1);
        $scope.updateCollection(section, collection);
      };

      $scope.uploadFiles = function(file, errFiles) {
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
          file.upload = Upload.upload({
            url: 'api/aidedPicture',
            data: {file: file, 'id': $scope.aided._id}
          });

          file.upload.then(function (response) {
            $timeout(function (){
              $scope.aided.profilePicture = response.data;
            });

          }, function (response) {
            if (response.status > 0)
              $scope.errorMsg = response.status + ': ' + response.data;
          }, function (evt) {
            file.progress = Math.min(100, parseInt(100.0 *
              evt.loaded / evt.total));
          });
        }
      };


    }]);
