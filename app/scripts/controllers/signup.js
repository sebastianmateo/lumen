'use strict';

angular.module('angularPassportApp')
	.controller('SignupCtrl', ['$scope', 'Auth', 'Invite', 'Groups', '$location', '$routeParams', function ($scope, Auth, Invite, Groups, $location, $routeParams) {
    $scope.initInvitation = function (form){
      Invite.get({
        key: $routeParams.key
      }, function (invite) {
        //Obtengo además las posadas
        Groups.query(function( groups ){
          $scope.groups = groups;
        });

        $scope.user = {};
        $scope.user.key = invite.hashedKey;
        $scope.user.email = invite.email;
        $scope.user.group = invite.group;
      });
    };

		$scope.register = function (form) {
			Auth.createUser({
					email: $scope.user.email,
					name: $scope.user.name,
					lastName: $scope.user.lastName,
					password: $scope.user.password
				},
				function (err) {
					$scope.errors = {};

					if (!err) {
						$location.path('/');
					} else {
						angular.forEach(err.errors, function (error, field) {
							form[field].$setValidity('mongoose', false);
							$scope.errors[field] = error.type;
						});
					}
				}
			);
		};
    $scope.registerPosadero = function (form) {
      Auth.createUser({
          email: $scope.user.email,
          name: $scope.user.name,
          lastName: $scope.user.lastName,
          password: $scope.user.password,
          key: $scope.user.key,
          group: $scope.user.group
        },
        function (err) {
          $scope.errors = {};

          if (!err) {
            $location.path('/');
          } else {
            angular.forEach(err.errors, function (error, field) {
              form[field].$setValidity('mongoose', false);
              $scope.errors[field] = error.type;
            });
          }
        }
      );
    };
	}]);
