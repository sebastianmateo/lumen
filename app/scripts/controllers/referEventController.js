'use strict';

angular.module('angularPassportApp')
  .controller('ReferEventController', ['$scope', 'Referral',
    function ($scope, Referral) {
      $scope.referrals = Referral.query();

    }]);
