'use strict';

angular.module('angularPassportApp')
  .controller('OtherProfileController', ['$scope', 'User', '$routeParams',
    function ($scope, User, $routeParams) {

      User.profile(
        { profileId: $routeParams.userId },
        function (user) {
          $scope.user = user;
        });
    }]);
