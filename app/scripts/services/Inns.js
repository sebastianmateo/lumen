'use strict';
//$resource define los metodos
/*
 get()
 query()
 save()
 remove()
 delete()
 */
angular.module('angularPassportApp')
	.factory('Inns', ['$resource', function ($resource) {
		return $resource('api/inns/:innId', {
			innId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}]);
