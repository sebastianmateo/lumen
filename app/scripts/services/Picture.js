'use strict';
//$resource define los metodos
/*
 get()
 query()
 save()
 remove()
 delete()
 */
angular.module('angularPassportApp')
	.factory('Picture', ['$resource', function ($resource) {
		return $resource('api/images/:imageId', {
			imageId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}]);