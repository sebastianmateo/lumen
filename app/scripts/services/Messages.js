'use strict';

angular.module('angularPassportApp')
  .factory('Messages', ['$rootScope', '$interval', 'Conversations', function ($rootScope, $interval, Conversations) {
    var new_messages = [];

    var hasUnreadMessages = function(conversation) 
    {
      var has_unread_messages = false;
      for (var message_key in conversation.messages)
      {
        var message = conversation.messages[message_key];
        if (message.read == false && (String(message.from) != String($rootScope.currentUser._id)))
          has_unread_messages = true;
      }

      return has_unread_messages;
    };

    var newMessagesCount = function() {
      return new_messages.length;
    };

    var requestMessages = function() {
      new_messages = [];
      Conversations.query(function (conversations) 
      {
        for (var conversation_key in conversations) {
          var conversation = conversations[conversation_key];
          for (var message_key in conversation.messages)
          {
            var message = conversation.messages[message_key];
            if (message.read === false && (String(message.from) != String($rootScope.currentUser._id))) {
              new_messages.push(message);
            }
          }
        }
        $rootScope.$broadcast('newMessages');
      })
    };

    return {
      newMessagesCount: newMessagesCount,
      requestMessages: requestMessages,
      hasUnreadMessages: hasUnreadMessages
    };
  }]);
