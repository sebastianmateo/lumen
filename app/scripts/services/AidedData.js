'use strict';

angular.module('angularPassportApp')
	.factory('AidedData', function () {
		function query() {
			return {
        personal: {
					maritalStatus: [
						"Soltero",
						"Casado",
						"Separado",
						"Viudo"
					],
					livingWith: [
						"Sólo",
						"Con su pareja",
						"Con sus hijos",
						"Con sus padres",
						"Con amigos"
					],
					residenceZone: [
						"CABA",
						"Provincia de Bs. As."
					]
				},
				clinical: {
					disabilities: [
						"Visual",
						"Auditiva",
						"Mental",
						"Motriz"
					],
                    diseases: {
                        "Hepatobiliares" : [
							"Litiasis Vesicular​ ​(C​á​lculos)",
							"Hígado graso",
							"Cirrosis",
							"Pancreatitis",
							"Otra"
						],
						"Digestivas e intestinales": [
							"Reflujo",
							"Úlcera gástrica",
							"Gastritis",
							"Hemorroides",
							"Celiaquía",
							"Pólipos de intestino",
							"Diarrea",
							"Otra"
						],
						"Dermatológicas": [
							"Eccema",
							"Psoriasis",
							"Úlceras de miembros inferiores",
							"Sarna",
							"Pediculosis",
							"Micosis de piel",
							"Micosis de uñas",
							"Micosis de cuero cabelludo",
							"Herpes Zoster (culebrilla)",
							"Herpes",
							"Otra"
						],
						"Cardiovasculares": [
							"Hipertensión",
							"Insuficiencia cardíaca",
							"Enfermedad coronaria",
							"Arritimia",
							"Otra"
						],
						"Broncopulmonares": [
							"Asma",
							"Bronquitis",
							"Neumonía",
							"EPOC",
							"Otra"
						],
						"Infecciosas": [
							"Tuberculosis",
							"Infección urinaria",
							"Hepatitis A-B-C",
							"Sífilis",
							"Gonorrea",
							"HIV",
							"Otra"
						],
						"Ginecológicas": [
							"Pérdida de embarazo",
							"HPV",
							"Herpes genital",
							"Flujo",
							"Otra"
						],
						"Renal y vías urinarias": [
							"Cólico renal",
							"Insuficiencia renal",
							"Litiasis Renal (Cálculos)",
							"Cistitis",
							"Prostatitis",
							"Pólipos de vejiga",
							"Otra"
						],
						"Óseas y articulares": [
							"Fracturas",
							"Artrosis de cadera",
							"Artrosis de rodilla",
							"Artrosis de hombro",
							"Artrosis de columna",
							"Osteoporosis",
							"Artritis",
							"Juanetes",
							"Malformaciones en dedos de pies",
							"Ciática",
							"Hernia de disco",
							"Otra"
						],
						"Otorrino-laringológicas": [
							"Otitis",
							"Hipoacusia",
							"Sordera",
							"Sinusitis",
							"Anginas",
							"Otras"
						],
						"Oftalmológicas": [
							"Cataratas",
							"Glaucoma",
							"Ceguera",
							"Visión disminuida",
							"Conjuntivitis",
							"Otra"
						],
						"Sistema nervioso": [
							"Cefaleas",
							"Epilepsia",
							"ACV",
							"Alzheimer",
							"Parkinson",
							"Wilson",
							"Otra"
						],
						"Odontológicas": [
							"Caries",
							"Pérdida de piezas dentarias",
							"Gingivitis",
							"Periodontitis",
							"Cáncer bucal",
							"Otra"
						],
						"Metabólicas y endocrinológicas": [
							"Diabetes",
							"Diabetes Insulino-dependiente",
							"Diabetes II",
							"Gota",
							"Hipertiroidismo",
							"Hipotiroidismo",
							"Hipercolesterolemia",
							"Obesidad",
							"Bajo peso",
							"Desnutrición",
							"Anorexia",
							"Otra"
						],
						"Cáncer": [
							"Mama",
							"Pulmón",
							"Leucemia",
							"Laringe",
							"Estómago",
							"Páncreas",
							"Próstata",
							"Útero",
							"Testículos",
							"Intestino",
							"Cavidad oral",
							"Piel",
							"Otro"
						]
                    },
                    medicalSpecialties: [
                        "Alergología",
						"Anestesiología y reanimación",
						"Cardiología",
						"Gastroenterología",
						"Endocrinología",
						"Geriatría",
						"Hematología y hemoterapia",
						"Hidrología médica",
						"Infectología",
						"Medicina aeroespacial",
						"Medicina del deporte",
						"Medicina del trabajo",
						"Medicina de urgencias",
						"Medicina familiar y comunitaria",
						"Medicina física y rehabilitación",
						"Medicina intensiva",
						"Medicina interna",
						"Medicina legal y forense",
						"Medicina preventiva y salud pública",
						"Nefrología",
						"Neumología",
						"Neurología",
						"Nutriología",
						"Oftalmología",
						"Oncología médica",
						"Oncología radioterápica",
						"Otorrinolaringología",
						"Pediatría",
						"Proctología",
						"Psiquiatría",
						"Rehabilitación",
						"Reumatología",
						"Traumatología",
						"Toxicología",
						"Urología"
                    ],
					treatmentStatus: [
						"Sí",
						"Suspendió el tratamiento",
						"Nunca estuvo bajo tratamiento"
					],
					treatmentAbandonmentReasons: [
						"No posee los medicamentos",
						"Reacciones adversas al medicamento",
						"Se olvida",
						"No le interesa seguir el tratamiento"
					],
                    routes : [
                        "Oral",
                        "Sublingual",
                        "Inhalatoria",
                        "Cutánea",
                        "Intramuscular",
                        "Intravenosa",
                        "Rectal"
                    ]
				},
				mental: {
					states: [
						"No presenta signos/síntomas mentales",
						"Presenta signos/síntomas mentales",
						"No se puede determinar"
					],
					signs: [
						"Ansiedad",
						"Estado depresivo",
						"Delirios",
						"Trastornos cognitivos",
						"Otros"
					],
                    disorders: [
                        "Esquizofrenia",
                        "Paranoia",
                        "Bipolaridad",
                        "Retraso mental",
                        "Autismo",
                        "Adicción",
                        "Depresión",
                        "Otra (aclarar en los detalles)"
                    ],
					conditionType: [
						"Transitoria",
						"Crónica"
					],
					speechType: [
						"Coherente",
						"Con fallos",
						"Delirante"
					],
					treatmentCategories: [
						"Ambulatorio",
						"Internación"
					],
					treatmentStatus: [
						"Cumple con el tratamiento",
						"No cumple con el tratamiento",
						"Cumple parcialmente",
						"Abandonó"
					],
					treatmentAbandonmentReasons: [
						"No posee los medicamentos",
						"Reacciones adversas al medicamento",
						"Se olvida",
						"No le interesa seguir el tratamiento"
					]
				},
				addictions: {
					drug: [
						"Alcohol",
						"Marihuana",
						"Paco",
						"Pegamento",
						"Cocaína",
						"Pastillas y Anfetaminas"
					],
                    frequency: [
                        "Varias veces por día",
                        "Diariamente",
                        "Varias veces por semana",
                        "Semanalmente",
                        "Ocasionalmente"
                    ],
					time: [
						"1 semana",
						"1 a 3 meses",
						"3 a 6 meses",
						"6 a 12 meses",
						"1 a 2 años",
						"Mas de 2 años"
					],
					treatmentType: [
						"Ambulatorio",
						"Internación"
					],
                    routes : [
                        "Oral",
                        "Sublingual",
                        "Inhalatoria",
                        "Cutánea",
                        "Intramuscular",
                        "Intravenosa",
						"Pulmonar",
                        "Rectal"
                    ]
				},
				socialServices: {
					type: [
						"Hogar",
						"Parador o Refugio",
						"Centro de Día",
						"Posadero (distinto al actual)",
						"Comedor",
                        "Bolsa de alimentos",
						"Duchas",
						"Ropería",
						"Subsidio Habitacional (Programa 690)",
						"Ticket Social",
						"Jubilación",
						"Pensión por discapacidad o invalidez",
						"Asignación Universal por Hijo",
						"Seguro de desempleo",
						"Bolsa de trabajo",
                        "Vivir en casa",
						"Otro"
					]
				},
				education: {
					completion: [
						"Completo",
						"Incompleto",
						"Nunca iniciado"
					]
				},
				donations: {
					type:[
						"Medicamentos",
						"Prótesis",
                        "Silla de ruedas",
						"Alimentos",
                        "Útiles escolares",
						"Ropa",
                        "Frazadas",
                        "Calzado",
                        "Materiales de construcción",
                        "Muebles",
						"Otro (especificar)"
					]
				},
                legal : {
                    branchesOfLaw: [
                        "Civil",
                        "Comercial",
                        "Laboral",
                        "Penal",
                        "Administrativo",
                        "Otra"
                    ]
                },
                innkeeper: {
                    actions: [
                        "En curso",
                        "Abandonada",
                        "Cumplida"
                    ]
                },
                family: {
                    relations: [
                        "Pareja",
                        "Hijo",
                        "Madre",
                        "Padre",
                        "Abuelo/a",
                        "Tío/a",
                        "Primo/a",
                        "Sobrino/a",
                        "Amigo/a"
                    ]
                },
                job: {
                    professionsAndTrades: [
                        "Abogado (especificar)",
                        "Acompañante Terapéutico",
                        "Actor",
                        "Actuario",
                        "Administración Empresas",
                        "Agrónomo",
                        "Analista en Sistemas",
                        "Arquitecto",
                        "Bellas Artes",
                        "Bibliotecario",
                        "Biólogo",
                        "Catequista",
                        "Ciencias Ambientales",
                        "Ciencias Comunicación",
                        "Ciencias de la Educación",
                        "Ciencias Políticas",
                        "Contador Público",
                        "Counselor",
                        "Despachante de Aduana",
                        "Diseñador (especificar)",
                        "Docente",
                        "Economista",
                        "Enfermero",
                        "Escribano",
                        "Farmacéutico",
                        "Filosofía",
                        "Físico",
                        "Floricultura",
                        "Historia",
                        "Hotelería",
                        "Ingeniero (especificar)",
                        "Instrumentador Quirúrgico",
                        "Kinesiólogo",
                        "Letras",
                        "Marketing",
                        "Martillero Público/ Corredor inmobiliario",
                        "Médico (especificar)",
                        "Músico",
                        "Notariado",
                        "Odontólogo",
                        "Paisajista",
                        "Periodista",
                        "Piloto",
                        "Policía",
                        "Profesor (especificar)",
                        "Programador",
                        "Psicólogo",
                        "Psicólogo Social",
                        "Psicopedagogo",
                        "Psiquiatra",
                        "Publicista",
                        "Químico",
                        "Recursos Humanos",
                        "Relaciones Internacionales",
                        "Relaciones Públicas",
                        "Sociólogo",
                        "Teólogo",
                        "Terapista Ocupacional",
                        "Trabajador Social",
                        "Traductor",
                        "Turismo",
                        "Veterinaria",
                        "Acomodador",
                        "Albañil",
                        "Almacenero",
                        "Artesano",
                        "Bancario",
                        "Barrendero",
                        "Bicicletero",
                        "Botones",
                        "Cafetero",
                        "Cajero",
                        "Camarógrafo",
                        "Camionero",
                        "Canillita",
                        "Carnicero",
                        "Carpintero",
                        "Cartero",
                        "Cartonero",
                        "Chofer",
                        "Cocinero",
                        "Colectivero",
                        "Delivery",
                        "Ebanista",
                        "Electricista",
                        "Ferroviario",
                        "Fletero",
                        "Florista",
                        "Fotógrafo",
                        "Fumigador",
                        "Gasista",
                        "Guardaparque",
                        "Guardavida",
                        "Heladero",
                        "Imprentero",
                        "Jardinero/ Parquero",
                        "Kiosquero",
                        "Lava-autos",
                        "Lavacopas/ Bachero",
                        "Luthier (instrumentos musicales)",
                        "Maestro Mayor de Obra",
                        "Masajista",
                        "Matricero",
                        "Mecánico",
                        "Mensajería privada",
                        "Mozo",
                        "Obrero",
                        "Orfebre",
                        "Panadero",
                        "Parrillero",
                        "Paseador de perros",
                        "Pedicuro/ Manicura/ Belleza",
                        "Peletero",
                        "Peluquero",
                        "Peón",
                        "Peón de campo",
                        "Personal de Limpieza",
                        "Personal de Vigilancia",
                        "Personal Trainer",
                        "Pintor",
                        "Pizzero",
                        "Platero",
                        "Plomero",
                        "Podólogo",
                        "Portero",
                        "Recolector de residuos",
                        "Reparador de PC",
                        "Repostero",
                        "Sastre/ Costurera",
                        "Secretaria",
                        "Sereno",
                        "Tapicero",
                        "Taxista",
                        "Telemarketer",
                        "Tintorero",
                        "Tornero",
                        "Valet Parking",
                        "Vendedor ambulante",
                        "Vendedor",
                        "Verdulero",
                        "Zapatero"
                    ]
                },
                aggressionCategories : [
                    "Física",
                    "Sexual"
                ],
                countries : [
                    "Argentina"
                    ,"Afganistán"
                    ,"Albania"
                    ,"Alemania"
                    ,"Andorra"
                    ,"Angola"
                    ,"Anguilla"
                    ,"Antigua y Barbuda"
                    ,"Antillas Holandesas"
                    ,"Arabia Saudita"
                    ,"Argelia"
                    ,"Armenia"
                    ,"Aruba"
                    ,"Australia"
                    ,"Austria"
                    ,"Azerbaiján"
                    ,"Bahamas"
                    ,"Bahrain"
                    ,"Bangladesh"
                    ,"Barbados"
                    ,"Bélgica"
                    ,"Belice"
                    ,"Benin"
                    ,"Bhutan"
                    ,"Bielorusia"
                    ,"Bolivia"
                    ,"Bosnia Herzegovina"
                    ,"Botswana"
                    ,"Brasil"
                    ,"Brunei"
                    ,"Bulgaria"
                    ,"Burkina Faso"
                    ,"Burundi"
                    ,"Cabo Verde"
                    ,"Camboya"
                    ,"Camerún"
                    ,"Canadá"
                    ,"Chad"
                    ,"Chile"
                    ,"China"
                    ,"Chipre"
                    ,"Colombi"
                    ,"Comoros"
                    ,"Congo"
                    ,"Corea"
                    ,"Costa de Marfil"
                    ,"Costa Rica"
                    ,"Croacia"
                    ,"Cuba"
                    ,"Darussalam"
                    ,"Dinamarca"
                    ,"Djibouti"
                    ,"Dominica"
                    ,"Ecuador"
                    ,"Egipto"
                    ,"El Salvador"
                    ,"Em. Arabes Un."
                    ,"Eritrea"
                    ,"Eslovaquia"
                    ,"Eslovenia"
                    ,"España"
                    ,"Estados Unidos "
                    ,"Estonia"
                    ,"Etiopía"
                    ,"Fiji"
                    ,"Filipinas"
                    ,"Finlandia"
                    ,"Francia"
                    ,"Gabón"
                    ,"Gambia"
                    ,"Georgia"
                    ,"Ghana"
                    ,"Gibraltar"
                    ,"Grecia"
                    ,"Grenada"
                    ,"Groenlandi"
                    ,"Guadalupe"
                    ,"Guam"
                    ,"Guatemala"
                    ,"Guayana Francesa"
                    ,"Guinea"
                    ,"Guinea Ecuatorial"
                    ,"Guinea-Bissau"
                    ,"Guyana"
                    ,"Haití"
                    ,"Holanda"
                    ,"Honduras"
                    ,"Hong Kong"
                    ,"Hungría"
                    ,"India"
                    ,"Indonesi"
                    ,"Irak"
                    ,"Irán"
                    ,"Irlanda"
                    ,"Islandia"
                    ,"Islas Cayman"
                    ,"Islas Cook"
                    ,"Islas Faro"
                    ,"Islas Marianas del Norte"
                    ,"Islas Marshall"
                    ,"Islas Solomon"
                    ,"Islas Turcas y Caicos"
                    ,"Islas Vírgenes"
                    ,"Islas Wallis y Futuna"
                    ,"Italia"
                    ,"Jamaica"
                    ,"Japón"
                    ,"Jordania"
                    ,"Kazajstán"
                    ,"Kenya"
                    ,"Kirguistán"
                    ,"Kiribati"
                    ,"Kuwait"
                    ,"Laos"
                    ,"Lesotho"
                    ,"Letonia"
                    ,"Líbano"
                    ,"Liberia"
                    ,"Libia"
                    ,"Liechtenstein"
                    ,"Lituania"
                    ,"Luxemburgo"
                    ,"Macao"
                    ,"Macedonia"
                    ,"Madagascar"
                    ,"Malasia"
                    ,"Malawi"
                    ,"Mali"
                    ,"Malta"
                    ,"Marruecos"
                    ,"Martinica"
                    ,"Mauricio"
                    ,"Mauritania"
                    ,"Mayotte"
                    ,"México"
                    ,"Micronesia"
                    ,"Moldova"
                    ,"Mónaco"
                    ,"Mongolia"
                    ,"Montserrat"
                    ,"Mozambique"
                    ,"Myanmar"
                    ,"Namibia"
                    ,"Nauru"
                    ,"Nepal"
                    ,"Nicaragua"
                    ,"Níger"
                    ,"Nigeria"
                    ,"Noruega"
                    ,"Nueva Caledonia"
                    ,"Nueva Zelandia"
                    ,"Omán"
                    ,"Pakistán"
                    ,"Palestina"
                    ,"Panamá"
                    ,"Papua Nueva Guinea"
                    ,"Paraguay"
                    ,"Perú"
                    ,"Pitcairn"
                    ,"Polinesia Francesa"
                    ,"Polonia"
                    ,"Portugal"
                    ,"Puerto Rico"
                    ,"Qatar"
                    ,"RD Congo"
                    ,"Reino Unido"
                    ,"República Centroafricana"
                    ,"República Checa"
                    ,"República Dominicana"
                    ,"Reunión"
                    ,"Rumania"
                    ,"Rusia"
                    ,"Rwanda"
                    ,"Sahara Occidental"
                    ,"Saint Pierre y Miquel"
                    ,"Samoa"
                    ,"Samoa Americana"
                    ,"San Cristóbal y Nevis"
                    ,"San Marino"
                    ,"Santa Elena"
                    ,"Santa Lucía"
                    ,"Sao Tomé y Príncipe"
                    ,"Senegal"
                    ,"Serbia y Montenegro"
                    ,"Seychelles"
                    ,"Sierra Leona"
                    ,"Singapur"
                    ,"Siria"
                    ,"Somalia"
                    ,"Sri Lanka"
                    ,"Sudáfrica"
                    ,"Sudán"
                    ,"Suecia"
                    ,"Suiza"
                    ,"Suriname"
                    ,"Swazilandia"
                    ,"Taiwán"
                ],
                refer: {
                    reason: [
                        "Adicciones",
                        "Alojameniento",
                        "Planes Sociales",
                        "DNI",
                        "Médico",
                        "Psicólogo o Psiquiatara",
                        "Embarazada en riesgo",
                        "Niño en riesgo",
                        "Capacidades diferentes",
                        "Violencia de género",
                        "Asesoramiento jurídico",
                        "Educación (Primaria o Secundaria)",
                        "Capacitación en oficios",
                        "Ayuda al Micro-emprendedor o artesano",
                        "Inmigrantes (Asistencia y Orientación)",
                        "Ortopedia",
                        "Medicamentos",
                        "Jubilación",
                        "Pensión por discapacidad",
                        "Hogares",
                        "Otros"
                    ]
                }
			}
		}

		return {
			query: query
		};
});

