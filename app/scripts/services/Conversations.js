'use strict';
/*
 get()
 query()
 save()
 remove()
 delete()
 */
angular.module('angularPassportApp')
	.factory('Conversations', ['$resource', function ($resource) {
		return $resource('/api/conversations/:conversationId/', {
				conversationId: '@_id'
			}, {
				'update': {
					method: 'PUT'
				}
			});
	}]);
