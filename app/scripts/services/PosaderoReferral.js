'use strict';
//$resource define los metodos
/*
 get()
 query()
 save()
 remove()
 delete()
 */
angular.module('angularPassportApp')
	.factory('PosaderoReferral', ['$resource', function ($resource) {
		return $resource('auth/refer/:referId', {
			transferId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}]);
