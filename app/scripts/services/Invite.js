'use strict';
//$resource define los metodos
/*
 get()
 query()
 save()
 remove()
 delete()
 */
angular.module('angularPassportApp')
  .factory('Invite', ['$resource', function ($resource) {
    return $resource('auth/invite/:key', {
      inviteId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }]);
