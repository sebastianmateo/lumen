'use strict';
//$resource define los metodos
/*
 get()
 query()
 save()
 remove()
 delete()
 */
angular.module('angularPassportApp')
	.factory('Aided', ['$resource', function ($resource) {
		return $resource('api/aided/:aidedId', {
			aidedId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}]);
