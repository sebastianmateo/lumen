'use strict';

angular.module('angularPassportApp')
  .factory('Events', ['$rootScope', '$interval', 'User', function ($rootScope, $interval, User) {
    var _events = [];

    var sections = {};
    sections['personal'] = 'Datos personales';
    sections['family'] = 'Familia';
    sections['mental'] = 'Salud mental';
    sections['clinical'] = 'Salud clínica';
    sections['addictions'] = 'Adicciones';
    sections['socialServices'] = 'Servicios sociales';
    sections['education'] = 'Educación';
    sections['job'] = 'Empleo';
    sections['legal'] = 'Legal';
    sections['donations'] = 'Donaciones';
    sections['innkeeper'] = 'Diagnóstico Posadero';

    var markEventsAsRead = function() {
      User.eventsRead();
    };

    var request_events = function(callback) {
      User.events(function (events)
      {
        var is_not_mine = function(event) {
          return String(event.userId) != String($rootScope.currentUser._id);
        };

        events = events.filter(is_not_mine);

        for (var key = 0; key < events.length; key++)
        {
          for (var section in events[key].changes)
          {
            events[key].section = sections[section];
          }

          events[key].time = new Date(events[key].date);
          events[key].aidedURL = '/aided/' + events[key].aidedId;
          events[key].userURL = '/profile/' + events[key].userId;
        }

        _events = events;

        callback(events);
      }.bind({callback:callback}))
    };

    return {
      markEventsAsRead : markEventsAsRead,
      requestEvents: request_events
    };
  }]);
