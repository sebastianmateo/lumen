'use strict';

angular.module('angularPassportApp')
	.factory('Sections', function () {
		function query() {
			return {
                sections: [
                    {
                        name: "personal",
                        title: "Datos personales"
                    },
                    {
                        name: "family",
                        title: "Familia"
                    },
                    {
                        name: "clinical",
                        title: "Salud Clínica"
                    },
                    {
                        name: "mental",
                        title: "Salud Mental"
                    },
                    {
                        name: "addictions",
                        title: "Adicciones"
                    },
                    {
                        name: "socialServices",
                        title: "Servicios Sociales"
                    },
                    {
                        name: "education",
                        title: "Educación"
                    },
                    {
                        name: "job",
                        title: "Empleo"
                    },
                    {
                        name: "legal",
                        title: "Legal"
                    },
                    {
                        name: "donations",
                        title: "Donaciones"
                    },
                    {
                        name: "innkeeper",
                        title: "Diagnóstico Posadero"
                    }
                ]}
    }
		return {
			query: query
		};
});

