'use strict';
//$resource define los metodos
/*
 get()
 query()
 save()
 remove()
 delete()
 */
angular.module('angularPassportApp')
	.factory('Tags', ['$resource', function ($resource) {
		return $resource('api/tags/:tagId', {
			tagId: '@_id'
		}, {
			update: {
				method: 'PUT'
			},
      search: {
        method: 'GET',
        isArray: true,
        params: {
          category: '@category'
        }
      }
		});
	}]);
