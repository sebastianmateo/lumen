'use strict';
//$resource define los metodos
/*
 get()
 query()
 save()
 remove()
 delete()
 */
angular.module('angularPassportApp')
	.factory('Groups', ['$resource', function ($resource) {
		return $resource('api/groups/:groupId', {
			groupId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}]);
