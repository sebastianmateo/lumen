'use strict';
/*
 get()
 query()
 save()
 remove()
 delete()
 */
angular.module('angularPassportApp')
	.factory('User', ['$resource', function ($resource) {
		return $resource('/auth/users/', {}, {
        'update': {
            url:'/auth/users/',
            method: 'PUT'
        },
        'get':{
          url: '/auth/users/profile',
          method: 'GET'
        },
        'profile': {
          url:'/auth/users/profile/:profileId',
          method: 'GET',
          param: 'profileId'
        },
        'events': {
          url:'/auth/users/events/',
          isArray: true,
          method: 'GET'
        },
        'posaderos': {
          url:'/auth/posaderos/',
          isArray: true,
          method: 'GET'
        },
        'voluntarios': {
          url:'/auth/voluntarios/',
          isArray: true,
          method: 'GET'
        },
		'eventsRead': {
		  url:'/auth/users/events/',
		  method: 'PUT'
		},
        'subscribe': {
          url:'/auth/users/subscribe/:subscribeAidedId',
          method: 'GET',
          param: 'subscribeAidedId'
        },
        'unsubscribe': {
          url:'/auth/users/unsubscribe/:subscribeAidedId',
          method: 'GET',
          param: 'subscribeAidedId'
        }
		});
}]);
