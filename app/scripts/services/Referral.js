'use strict';
//$resource define los metodos
/*
 get()
 query()
 save()
 remove()
 delete()
 */
angular.module('angularPassportApp')
	.factory('Referral', ['$resource', function ($resource) {
		return $resource('api/refer/:referId', {
			transferId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}]);
