'use strict';
//$resource define los metodos
/*
 get()
 query()
 save()
 remove()
 delete()
 */
angular.module('angularPassportApp')
	.factory('Blogs', ['$resource', function ($resource) {
		return $resource('api/blogs/:blogId', {
			blogId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}]);
