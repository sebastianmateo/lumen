'use strict';

angular.module('angularPassportApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ngLocale',
  'http-auth-interceptor',
  'ui.bootstrap',
  'ngFileUpload',
  'xeditable'
])
  .config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/main', {
        templateUrl: 'partials/main.html'
      })
      .when('/profile', {
        templateUrl: 'partials/profile/profile.html',
        controller: 'ProfileController'
      })
      .when('/profile/:userId', {
        templateUrl: 'partials/profile/profile.view.html',
        controller: 'OtherProfileController'
      })
      .when('/inns', {
        templateUrl: 'partials/inns/inns.html',
        controller: 'InnController'
      })
      .when('/inns/:innId', {
        templateUrl: 'partials/inns/inns.edit.html',
        controller: 'InnController'
      })
      .when('/gender', {
        templateUrl: 'partials/tags/gender.html',
        controller: 'TagController'
      })
      .when('/gender/:tagId', {
        templateUrl: 'partials/tags/gender.edit.html',
        controller: 'TagController'
      })
      .when('/professions', {
        templateUrl: 'partials/tags/professions.html',
        controller: 'TagController'
      })
      .when('/professions/:tagId', {
        templateUrl: 'partials/tags/professions.edit.html',
        controller: 'TagController'
      })
      .when('/populations/:tagId', {
        templateUrl: 'partials/tags/populations.edit.html',
        controller: 'TagController'
      })
      .when('/populations', {
        templateUrl: 'partials/tags/populations.html',
        controller: 'TagController'
      })
      .when('/thematic/:tagId', {
        templateUrl: 'partials/tags/thematicAreas.edit.html',
        controller: 'TagController'
      })
      .when('/thematic', {
        templateUrl: 'partials/tags/thematicAreas.html',
        controller: 'TagController'
      })
      .when('/aided.add', {
        templateUrl: 'partials/aided/aided.add.html',
        controller: 'CreateAidedController'
      })
      .when('/aided', {
        templateUrl: 'partials/aided/aided.html',
        controller: 'AidedController'
      })
      .when('/aided/:aidedId', {
        templateUrl: 'partials/aided/aided.view.html',
        controller: 'AidedController'
      })
      .when('/aided.refer', {
        templateUrl: 'partials/aided/aided.refer.html',
        controller: 'ReferController'
      })
      .when('/users', {
        templateUrl: 'partials/users/users.view.html',
        controller: 'UserController'
      })
      .when('/users.invite', {
        templateUrl: 'partials/users/users.invite.html',
        controller: 'UserInviteController'
      })
      .when('/conversations', {
        templateUrl: 'partials/conversations/conversations.html',
        controller: 'ConversationController'
      })
      .when('/conversations.new/:userId', {
        templateUrl: 'partials/conversations/conversations.new.html',
        controller: 'ConversationController'
      })
      .when('/conversations/:conversationId', {
        templateUrl: 'partials/conversations/conversations.view.html',
        controller: 'ConversationController'
      })
      .when('/login', {
        templateUrl: 'partials/login.html',
        controller: 'LoginCtrl'
      })
      .when('/signup', {
        templateUrl: 'partials/signup.html',
        controller: 'SignupCtrl'
      })
      .when('/signup/:key', {
        templateUrl: 'partials/signupPosadero.html',
        controller: 'SignupCtrl'
      })
      .otherwise({
        redirectTo: '/profile'
      });
    $locationProvider.html5Mode(true);
  }])

  .run(function(editableOptions) {
    editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
  })

  .run(['$rootScope', '$location', 'Auth', function ($rootScope, $location, Auth) {
    //watching the value of the currentUser variable.
    $rootScope.$watch('currentUser', function (currentUser) {
      // if no currentUser and on a page that requires authorization then try to update it
      // will trigger 401s if user does not have a valid session
      var isValidPath = false;
      var validPaths = [/^\/$/, /^\/login$/, /^\/logout$/, /^\/signup$/, /^\/signup\/[0-9a-zA-Z]*$/];

      var path = $location.path();

      for (var i = 0; i < validPaths.length; i++) {
        if (path.search(validPaths[i]) >= 0) {
          isValidPath = true;
          break;
        }
      }

      //Si el path no es válido, y no está logueado, busco autenticar al usuario
      if (!currentUser && !isValidPath) {
        Auth.currentUser();
      }

      //User not logged, and /
      if (!currentUser && path.search(/^\/$/) >= 0){
        $location.path('/login');
        return false;
      }

    });

    // On catching 401 errors, redirect to the login page.
    $rootScope.$on('event:auth-loginRequired', function () {
      $location.path('/login');
      return false;
    });
  }]);
