db = db.getSiblingDB("test");

var PROFESSION = 0, POPULATION = 1, THEMATIC_AREA = 2, GENDER = 3;

//Profesiones
db.tags.insert({name: "Abogado", category: PROFESSION });
db.tags.insert({name: "Acompañante Terapéutico", category: PROFESSION });
db.tags.insert({name: "Comunicador", category: PROFESSION });
db.tags.insert({name: "Médico", category: PROFESSION });
db.tags.insert({name: "Psicólogo", category: PROFESSION });
db.tags.insert({name: "Psiquiatra", category: PROFESSION });
db.tags.insert({name: "Sociólogo", category: PROFESSION });
db.tags.insert({name: "Terapista Ocupacional", category: PROFESSION });
db.tags.insert({name: "Trabajador Social", category: PROFESSION });

//Poblaciones
db.tags.insert({name: "Niños", category: 1 });
db.tags.insert({name: "Adolescentes", category: 1 });
db.tags.insert({name: "Jóvenes adultos", category: 1 });
db.tags.insert({name: "Adultos mayores", category: 1 });
db.tags.insert({name: "Tercera edad", category: 1 });

//Áreas Temáticas
db.tags.insert({name: "Salud", category: 2 });
db.tags.insert({name: "Educación", category: 2 });
db.tags.insert({name: "Adicciones", category: 2 });
db.tags.insert({name: "Niñez y adolescencia", category: 2 });
db.tags.insert({name: "Familia", category: 2 });
db.tags.insert({name: "Mujer", category: 2 });
db.tags.insert({name: "Tercera edad", category: 2 });
db.tags.insert({name: "Empleo", category: 2 });
db.tags.insert({name: "Personas en situación de calle" });
db.tags.insert({name: "Orientación al ciudadano" });

//Genero
db.tags.insert({name: "Masculino", category: 3 });
db.tags.insert({name: "Femenino", category: 3 });
