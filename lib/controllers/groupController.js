'use strict';

var mongoose = require('mongoose'),
  Inn = mongoose.model('Inn'),
  Group = mongoose.model('Group')

//Cargo el tag para que después en otros querys lo pueda usar
exports.group = function(req, res, next, id) {
  Group.load(id, function(err, group) {
    if (err) return next(err);
    if (!group) return next(new Error('Failed to load group' + id));
    req.group = group;
    next();
  });
};

//List of groups
exports.all = function (req, res) {
  Group.find()
    .populate('inn', 'name')
    .exec(function (err, inns) {
      if (err) {
        res.json(500, err);
      } else {
        res.json(inns);
      }
    });
};

exports.create = function(req, res) {
	var group = new Group({
    name: req.body.name,
    day: req.body.day,
    time: req.body.time,
    inn: req.body.inn
  });

	group.save(function(err) {
		if (err) {
			res.json(500, err);
		} else {
      //Si tiene padre, lo agregoif (typeof req.body.parent != 'undefined') {
      Inn.update({_id: req.body.inn}, {$addToSet: {groups: group._id}}, {}, function (err) {
          if (err) {
            res.json(500, err);
          } else {
            res.json(group);
          }
        });
      }
		});
};

exports.destroy = function(req, res) {
    var group = req.group;

    group.remove(function(err) {
        if (err) {
            res.json(500, err);
        } else {
          //Lo elimino de la lista del padre
          Inn.update({_id: group.inn}, {$pull: {groups: group._id}}, {}, function (err) {
            if (err) {
                res.json(500, err);
              } else {
                res.json(group);
              }
            });
          }
        });
};

exports.update = function(req, res) {
  var group = req.group;
  group.name = req.body.name;
  group.time = req.body.time;
  group.day = req.body.day;

  group.save(function(err) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(group);
    }
  });
};
