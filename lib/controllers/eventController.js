'use strict';

var mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Aided = mongoose.model('Aided'),
  events = require('events');

exports.eventEmitter = new events.EventEmitter();

//List of Events for a user
exports.mine = function (req, res) {
  var user = req.user;
  var events = user.get('events');
  res.json(events);
  //user.set('events', events);
};

//List of Events for a user
exports.markAsRead = function (req, res) {
  var user = req.user;

  if(user.events)
  {
    user.events.forEach(function(event)
    {
      event.read = true;
    });

    user.save();
  }
  res.json(200);
};

exports.subscribe = function(req, res) {
  var user = req.user;
  var aidedId = req.params.subscribeAidedId;
  user.aided.push(aidedId);
  user.save(function(err) {
    if (err) {
      console.log("Error agregando aided");
      res.json(500, err);
    } else {
      //Agregamos el asistido al usuario
      Aided.update({_id: aidedId}, {$addToSet: {users: req.user._id}}, {}, function (err) {
        if (err) {
          console.log("Error agregando hijo")
          res.json(500, err);
        } else {
          res.json(200);
        }
      });
    }
  });
};

exports.unsubscribe = function(req, res) {
  var user = req.user;
  var aidedId = req.params.subscribeAidedId;

  //Remove the element from the aided list
  var index = user.aided.indexOf(aidedId);
  if (index > -1) {
    user.aided.splice(index, 1);
  } else {
    res.json(500);
  }

  user.save(function(err) {
    if (err) {
      console.log("Error agregando aided");
      res.json(500, err);
    } else {
      //Agregamos el asistido al usuario
      Aided.update({_id: aidedId}, {$pull: {users: req.user._id}}, {}, function (err) {
        if (err) {
          console.log("Error eliminando hijo")
          res.json(500, err);
        } else {
          res.json(200);
        }
      });
    }
  });
};

exports.event = function(user, event) {
  //Obtenemos todos los usuarios que están observando al asistido
  Aided.findOne({_id: event.aidedId}, 'users',
    function(err, aided){
      if (err){
        console.log(err);
      } else {
        /*
         var index = aided.users.indexOf(user._id);
         if (index > -1) {
         aided.users.splice(index, 1);
         }
         */
        event.date = new Date();
        User.update( {_id : {"$in":aided.users}}, {$addToSet: {events: event}} , {multi: true} , function(err) {
          if (err){
            console.log(err);
          }
        });
      }
    }
  );
};
