'use strict';

var mongoose = require('mongoose'),
  Tag = mongoose.model('Tag'),
  constants = require('./constants.js');

//Cargo el tag para que después en otros querys lo pueda usar
exports.tag = function(req, res, next, id) {
    Tag.load(id, function(err, tag) {
        if (err) return next(err);
        if (!tag) return next(new Error('Failed to load tag' + id));
        req.tag = tag;
        next();
    });
};

//List of Tags
exports.all = function (req, res) {
  if (typeof req.query.category != 'undefined') {
    Tag.find({ 'category' : constants.Category[req.query.category]})
      .populate('subtags')
      .exec(function (err, tags) {
        if (err) {
          res.json(500, err);
        } else {
          res.json(tags);
        }
      });
  } else {
    Tag.find()
      .exec(function (err, tags) {
        if (err) {
          res.json(500, err);
        } else {
          res.json(tags);
        }
      });
  }

};

exports.create = function(req, res) {
  console.log("Creando tag");
	var tag = new Tag({
    name: req.body.name,
    category: constants.Category[req.body.category]
  });
  //If it has a parent, we add it
  if (typeof req.body.parent != 'undefined') {
    tag.parent = req.body.parent;
  }

	tag.save(function(err) {
		if (err) {
      console.log("ERROR");
			res.json(500, err);
		} else {
      console.log("Viendo si tiene parent");
      //Si tiene padre, lo agrego
      if (typeof req.body.parent != 'undefined') {
        console.log("Adding parent");
        tag.parent = req.body.parent;
        Tag.update({_id: req.body.parent}, {$addToSet: {subtags: tag._id}}, {}, function (err) {
          if (err) {
            console.log("Error agregando hijo")
            res.json(500, err);
          } else {
            res.json(tag);
          }
        });
      } else {
        res.json(tag);
      }
		}
	});
};

//Mostrar una profesion
exports.show = function(req, res) {
	res.json(req.tag);
};

exports.destroy = function(req, res) {
    var tag = req.tag;

    tag.remove(function(err) {
        if (err) {
            res.json(500, err);
        } else {
          //Si tiene padre, lo elimino de su lista
          if (typeof tag.parent != 'undefined') {
            console.log("Eliminando al hijo de la lista");
            Tag.update({_id: tag.parent}, {$pull: {subtags: tag._id}}, {}, function (err) {
              if (err) {
                console.log("Error al eliminar");
                res.json(500, err);
              } else {
                console.log("Eliminado");
                res.json(tag);
              }
            });
          } else {

            res.json(tag);
          }
        }
    });
};

exports.update = function(req, res) {
  var tag = req.tag;
  tag.name = req.body.name;
  tag.sections = req.body.sections;

  tag.save(function(err) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(tag);
    }
  });
};
