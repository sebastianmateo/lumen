'use strict';

var mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Invite = mongoose.model('Invite'),
  Inn = mongoose.model('Inn'),
  Group = mongoose.model('Group'),
  Tag = mongoose.model('Tag'),
  nodemailer = require('nodemailer'),
  constants = require('./constants.js');

//Cargo el usuario para que después en otros querys lo pueda usar
exports.authUser = function(req, res, next) {
  var id = req._passport.session.user;
  User.load(id, function(err, user) {
    if (err) return next(err);
    if (!user) return next(new Error('Failed to load user ' + id));
    req.user = user;
    next();
  });
};

//Cargo el usuario para que después en otros querys lo pueda usar
exports.profile = function(req, res, next, id) {
  User.profile(id, function(err, user) {
    if (err) return next(err);
    if (!user) return next(new Error('Failed to load user ' + id));

    if (typeof user.group != 'undefined'){
      Inn.findById(user.group.inn, function (err, inn) {
        if (err) return next(err);
        user.inn = inn.name;
        req.user = user;
        next();
      });
    } else {
      req.user = user;
      next();
    }
  });
};

//Lista de posaderos
exports.posaderos = function(req, res) {
  User
    .find({role:constants.Role.POSADERO})
    .select('name lastName role profilePicture group')
    .exec(function (err, users){
      if (err){
        res.json(500, err);
      } else {
        res.json(users);
      }
    });
};

//Lista de voluntarios
exports.voluntarios = function(req, res) {
  User
    .find({role:constants.Role.VOLUNTARIO})
    .select('name lastName role profilePicture group')
    .exec(function (err, users){
      if (err){
        res.json(500, err);
      } else {
        res.json(users);
      }
    });
};

//Lista de Posaderos y Voluntarios
exports.all = function (req, res) {
  User
  .find({role:{$in:[1,2]}})
  .populate('group tags')
  .select('name lastName role profilePicture group tags')
  .exec(function (err, users)
  {
    if (err)
	{
      res.json(500, err);
    }
	else
	{
	  Group.populate(users, {
		path: 'group.inn',
		model: 'Inn'
	  }, function (err, users) {
		if (err) return next(err);
		Tag.populate(users, {
		  path: 'tags.subtags',
		  model: 'Tag'
		}, function(err, users) {
	  		if (err) return next(err);
			res.json(users);
		});
	  });
	}
  });
};

exports.validatePosadero = function (req, res, next){
  //Si el usuario tiene una key está intentando registrarse como Posadero
  if (req.body.key != null){
    Invite.findOne({
      'email': req.body.email,
      'hashedKey': req.body.key
    })
      .exec(function (err, invite) {
        if (err) {
          res.json(500, err);
        }
        if ( invite ){
          req.body.email = invite.email;
          req.body.role = constants.Role.POSADERO;
          next();
        } else {
          return res.json(400, err);
        }
      });
    //Sinó el usuario es un voluntario, se le permite registrarse
  } else {
    req.body.role = constants.Role.VOLUNTARIO;
    next();
  }
};
/**
 * Create user
 * requires: {name, lastName, password, email, profession}
 * returns: {email, password}
 */
exports.create = function (req, res, next) {
  var newUser = new User({
    email: req.body.email,
    name: req.body.name,
    lastName: req.body.lastName,
    password: req.body.password,
    role: req.body.role
  });
  if (req.body.group) newUser.group = req.body.group._id;

  newUser.provider = 'local';
  newUser.save(function (err) {
    if (err) {
      console.log(err);
      return res.json(400, err);
    }

    req.logIn(newUser, function (err) {
      if (err) return next(err);
      return res.json(newUser.user_info);
    });
  });
};

exports.updatePicture = function (req, res, next) {
  var user = req.user;
  user.profilePicture = req.files.file.name;

  user.save(function(err) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(req.files.file.name);
    }
  });
};

exports.update = function (req, res, next) {
  var user = req.user;

  //Agrego los tags si existen
  user.tags = req.body.tags;
  user.name = req.body.name;
  user.lastName = req.body.lastName;
  user.province = req.body.province;
  user.city = req.body.city;
  user.institution = req.body.institution;

  //Agrego el grupo si existe
  user.group = req.body.group;

  user.save(function(err) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(user);
    }
  });
};


/**
 *  Show profile
 *  returns {user}
 */
exports.show = function (req, res, next) {
  res.send(req.user);
};

exports.sendMail = function(req, res, next) {
  //Configuración de DEV para que no mande constantemente mails
  if (!constants.sendMail)
    return res.json();

  //Send a mail inviting the user
  var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: { user: 'devlumen@gmail.com', pass: 'bobbytables' }
  });

  var mailOptions = {
    from: 'devlumen@gmail.com', // sender address
    to: req.body.email, // list of receivers
    subject: 'Registro de Posadero', // Subject line
    html: '<p>Para registrarse, ingrese en: <a href="http://localhost:9000/signup/' + req.body.hashedKey + '">Link</a><p>'
  };

  transporter.sendMail(mailOptions, function(error, info){
    if(error){
      console.log(error);
      res.json(400, error);
    }else{
      console.log('Message sent: ' + info.response);
      return res.json();
    }
  })
};

exports.invite = function(req, res, next) {
  //Me fijo primero si hay una invitación para este usuario
  var email = req.body.email;
  var group = req.body.group;

  var hashedKey = "";

  //Busco si el usuario ya está registrado
  User.findOne({
    'email': email
  }).exec(function (err, user) {
    if (err) {
      res.json(500, err);
    }

    if (user) {
      res.json(500, err);
    } else {
      Invite.findOne({
        'email': email
      }).exec(function (err, invite) {
        if (err) {
          res.json(500, err);
        }

        //Si ya existe, le vuelvo a mandar la invitación
        if (invite) {
          req.body.hashedKey = invite.hashedKey;
          next();
        } else {
          //Creo un usuario
          var newInvite = new Invite({
            email: email,
            group: group,
            date: new Date(),
            key: req.body.email,
            status: constants.InviteStatus.PENDING
          });

          newInvite.save(function (err) {
            if (err) {
              console.log(err);
              return res.json(400, err);
            } else {
              req.body.hashedKey = newInvite.hashedKey;
              next();
            }
          })
        }
      })
    }
  })
};

exports.pendingInvites= function (req, res, next) {
  Invite.find({status: constants.InviteStatus.PENDING})
    .select('email date -_id')
    .exec(function (err, invites) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(invites);
    }
  });
};

//Página de SignUp para Usuario Posadero
exports.showInvite = function (req, res, next) {
  if (req.params.key != null){
    Invite.findOne({
      'hashedKey': req.params.key,
      'status': constants.InviteStatus.PENDING
    })
      .populate('group')
      .exec(function (err, invite) {
        if (err) {
          res.json(401, err);
        }
        if ( invite ){
          res.json(invite);
        } else {
          return res.json(401, err);
        }
      })
  } else {
    return res.json(401);
  }
};
