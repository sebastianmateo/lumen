'use strict';

var mongoose = require('mongoose'),
  Conversation = mongoose.model('Conversation');

//Obtengo todos los mensajes asociados a un usuario
exports.mine = function (req, res) {
  Conversation.find( { 'participants' : req.user._id})
    //.select('participants sent subject lastSent')
    .populate('participants', 'name lastName profilePicture')
    .sort([['lastSent','descending']])
    .exec(function (err, conversations) {
      if (err) {
        res.json(500, err);
      } else {
        res.json(conversations);
      }
    });
};

//Creo un mensaje entre 2 usuarios
exports.create = function(req, res) {
  var lastSent = new Date();

  var conversation = new Conversation({
    participants:[ req.user._id, req.body.to],
    subject: req.body.subject,
    sent: lastSent,
    lastSent: lastSent,
    messages: [{
      from: req.user._id,
      content: req.body.content,
      sent: new Date(),
      read: false
    }]
  });

  conversation.save(function(err) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(conversation);
    }
  });
};

//Cargo la conversación
exports.conversation = function(req, res, next, id) {
  Conversation.load(id, function(err, conversation) {
    if (err) return next(err);
    if (!conversation) return next(new Error('Failed to load conversation' + id));
    req.conversation = conversation;
    next();
  });
};

exports.show = function (req, res, next) {
  var conversation = req.conversation;
  console.log(req.conversation.messages);

  for (var i = 0; i < req.conversation.messages.length; i++ )
  {
    var message = req.conversation.messages[i];
    if (String(req.user._id) != String(message.from))
      message.read = true;
  }

  conversation.save(function(err) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(conversation);
    }
  });
};

exports.send = function (req, res, next) {
  var conversation = req.conversation;
  var lastSentDate = new Date();

  conversation.lastSent = lastSentDate;
  conversation.messages.push({
    from: req.user._id,
    content: req.body.answer,
    sent: lastSentDate,
    read: false
  });

  conversation.save(function(err) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(conversation);
    }
  });
};
