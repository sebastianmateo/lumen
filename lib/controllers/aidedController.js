'use strict';

var mongoose = require('mongoose'),
  Aided = mongoose.model('Aided'),
  Tag = mongoose.model('Tag'),
  constants = require('./constants.js'),
  eventEmitter = require('./eventController.js').eventEmitter;

function arrayUnique(array) {
    var a = array.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
}

//Cargo el asistido con las secciones a las que puede acceder el usuario
exports.aided = function(req, res, next, id) {
   //Busco las profesiones del usuario
    Tag.find({_id: {$in: req.user.tags}})
        .exec(function (err, tags) {
        if (err) {
            res.json(500, err);
        } else {
            var sections = [];
            for (var i = 0; i < tags.length; ++i) {
                //Concateno las secciones a las que puede acceder
                sections = arrayUnique(sections.concat(tags[i].sections));
            }
            //Ahora si cargo el asistido usando las secciones que tiene permitido
            Aided.load(id, sections, function(err, aided) {
                if (err) return next(err);
                if (!aided) return next(new Error('Failed to load aided ' + id));
                req.aided = aided;
                next();
            });

        }
    });


};

//List of Aided
exports.all = function (req, res) {
	Aided.find()
        .select('_id name lastName dni profilePicture')
        .exec(function (err, aided) {
		if (err) {
			res.json(500, err);
		} else {
			res.json(aided );
		}
	});
};

/**
 * Creación de Asistido para un Usuario Posadero
 * @param aided(name, lastName, dni), user(group)
 * @return aided._id
 */
exports.create = function(req, res) {
  var aided = new Aided({
    name: req.body.name,
    lastName: req.body.lastName,
    dni: req.body.dni,
    group: req.user.group
  });

  aided.save(function(err, aided) {
    if (err) {
      res.json(500, err);
    } else {
      res.json({"aidedId": aided._id});
    }
  });
};

//Obtengo UN asistido
exports.show = function(req, res) {
    res.json(req.aided);
};

exports.update = function(req, res) {
	var aided = req.aided;
    // Basic information
	if (typeof req.body.name != 'undefined') { aided.name = req.body.name; }
	if (typeof req.body.lastName != 'undefined') { aided.lastName = req.body.lastName; }
	if (typeof req.body.dni != 'undefined') { aided.dni = req.body.dni; }

	for (var section in req.body)
	{
		for (var property in req.body[section])
		{
			aided[section][property] = req.body[section][property];
		}
	}

	aided.save(function(err) {
		if (err) {
			res.json(500, err);
		} else {
      eventEmitter.emit('event:aided_updated', req.user, {
        category: constants.EventCategory.AIDED_UPDATE,
        aidedId : aided._id,
        aidedName : aided.name + ' ' + aided.lastName,
        userId: req.user._id,
        userName: req.user.name + ' ' + req.user.lastName,
        changes: req.body
      } );

			res.json(aided);
		}
	});
};

exports.updatePicture = function (req, res, next) {
    var aidedId = req.body.id;
    Aided.update({_id: aidedId}, {profilePicture: req.files.file.name}, function (err, aided) {
        if (err) {
            console.log(err);
            res.json(500, err);
        } else {
            res.json(req.files.file.name);
        }
    });
};
