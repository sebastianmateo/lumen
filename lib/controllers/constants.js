exports.InviteStatus = {
  PENDING: 0,
  CANCELLED: 1,
  ACCEPTED: 2
};

exports.Role = {
  ADMIN: 0,
  POSADERO: 1,
  VOLUNTARIO: 2
};

exports.sendMail = true;

exports.Category = {
  PROFESSION: 0,
  POPULATION: 1,
  THEMATIC_AREA: 2,
  GENDER: 3,
  PROFESSION_SPECIALIZATION: 4,
  THEMATIC_AREA_SPECIALIZATION: 5,
};

exports.EventCategory = {
  AIDED_UPDATE: 0,
}
