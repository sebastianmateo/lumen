'use strict';

var mongoose = require('mongoose'),
Aided = mongoose.model('Aided'),
User = mongoose.model('User'),
Refer = mongoose.model('Refer');

exports.create = function(req, res) {
  //Creo el asistido con los datos que me pasaron
  var aided = new Aided({
    name: req.body.name,
    lastName: req.body.lastName,
    dni: req.body.dni,
    group: req.body.group._id,
    profilePicture: req.body.profilePicture
  });

  aided.save(function(err) {
    if (err) {
      res.json(500, err);
    } else {
      //Si lo pude guardar, entonces guardo el refer
      var refer = new Refer({
        referReasons: req.body.referReasons,
        necessities: req.body.necessities,
        aided: aided,
        group: req.body.group._id,
        sourceUser: req.user._id,
        date: new Date()
      });
      refer.save(function(err) {
        if (err){
          console.log(err);
          res.json(500, err);
        } else {
          res.json();
        }
      })
    }
  });
};

exports.upload = function(req, res) {
  var file = req.files.file;
};

//Obtengo todos los mensajes asociados a un usuario
exports.mine = function (req, res) {
  Refer.find( { $or:[ {'group':req.user.group}, {'targetUser' : req.user._id}] })
    .populate('sourceUser', 'name lastName _id')
    .populate('aided', 'name lastName _id')
    .exec(function (err, refers) {
      if (err) {
        res.json(500, err);
      } else {
        res.json(refers);
      }
    });
};

//Que un posadero refiera un asistido a otro posadero
exports.refer = function (req, res) {
  var aidedId = req.body.referId;
  var targetPosaderoId = req.body.targetPosaderoId;

  //Primero creamos un Refer
  var refer = new Refer({
    aided: aidedId,
    targetUser: targetPosaderoId,
    sourceUser: req.user._id,
    date: new Date()
  });

  refer.save(function(err) {
    if (err){
      console.log(err);
      res.json(500, err);
    } else {
      User.update(
        {_id: targetPosaderoId},
        {$addToSet: {aided: aidedId}},
        function (err) {
          if (err) {
            console.log("Error agregando aided");
            res.json(500, err);
          } else {
            Aided.update(
              {_id: aidedId},
              {$addToSet: {users: targetPosaderoId}},
              function (err) {
                if (err) {
                  console.log("Error agregando hijo");
                  res.json(500, err);
                } else {
                  res.json(200);
                }
              }
            )
          }
        }
      )
    }
  })
};
