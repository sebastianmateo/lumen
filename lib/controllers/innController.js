'use strict';

var mongoose = require('mongoose'),
  Inn= mongoose.model('Inn'),
  Group = mongoose.model('Group')

//Cargo el tag para que después en otros querys lo pueda usar
exports.inn = function(req, res, next, id) {
    Inn.load(id, function(err, inn) {
        if (err) return next(err);
        if (!inn) return next(new Error('Failed to load inn' + id));
        req.inn = inn;
        next();
    });
};

//List of Tags
exports.all = function (req, res) {
    Inn.find()
      .populate('groups')
      .exec(function (err, inns) {
        if (err) {
          res.json(500, err);
        } else {
          res.json(inns);
        }
      });
  };

exports.create = function(req, res) {
	var inn = new Inn({
    name: req.body.name,
    phone: req.body.phone,
    address: req.body.address
  });

  inn.save(function(err) {
		if (err) {
			res.json(500, err);
		} else {
        res.json(inn);
      }
		});
};

//Mostrar una posada
exports.show = function(req, res) {
	res.json(req.inn);
};

exports.destroy = function(req, res) {
    var inn = req.inn;

    inn.remove(function(err) {
        if (err) {
            console.log(err);
            res.json(500, err);
        } else {
            res.json(inn);
        }
    });
};

exports.update = function(req, res) {
  var inn = req.inn;
  inn.name = req.body.name
  inn.phone = req.body.phone
  inn.address = req.body.address

  inn.save(function(err) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(inn);
    }
  });
};
