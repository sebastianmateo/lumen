'use strict';

var path = require('path'),
  auth = require('../config/auth'),
  eventEmitter = require('../controllers/eventController.js').eventEmitter;

module.exports = function (app) {
  // User Routes
  var userController = require('../controllers/userController');
  var eventController = require('../controllers/eventController');

  app.get('/auth/posaderos', auth.ensureAuthenticated, userController.posaderos);
  app.get('/auth/voluntarios', auth.ensureAuthenticated, userController.voluntarios);
  app.get('/auth/users', auth.ensureAuthenticated, userController.all);
  app.post('/auth/users', userController.validatePosadero, userController.create); //Crear usuario
  app.get('/auth/users/profile', auth.ensureAuthenticated, userController.authUser, userController.show);
  app.get('/auth/users/profile/:profileId', auth.ensureAuthenticated, userController.show); //Other user profile
  app.put('/auth/users/', auth.ensureAuthenticated, userController.authUser, userController.update);

  //Events
  app.get('/auth/users/events/', auth.ensureAuthenticated, auth.isPosadero, userController.authUser, eventController.mine);
  app.put('/auth/users/events/', auth.ensureAuthenticated, auth.isPosadero, userController.authUser, eventController.markAsRead);
  eventEmitter.on('event:aided_updated', eventController.event);
  app.get('/auth/users/subscribe/:subscribeAidedId', auth.ensureAuthenticated, auth.isPosadero, userController.authUser, eventController.subscribe);
  app.get('/auth/users/unsubscribe/:subscribeAidedId', auth.ensureAuthenticated, auth.isPosadero, userController.authUser, eventController.unsubscribe);

  app.param('profileId', userController.profile);

  //Invites route
  app.post('/auth/invite', auth.ensureAuthenticated, auth.isAdmin, userController.invite, userController.sendMail);
  app.get('/auth/invite', auth.ensureAuthenticated, auth.isAdmin, userController.pendingInvites);
  app.get('/auth/invite/:key', userController.showInvite); //Página de Signup de un Posadero

  //Tag route
  var tagController = require('../controllers/tagController');
  app.get('/api/tags', auth.ensureAuthenticated, tagController.all);
  app.get('/api/tags/:tagId', tagController.show);
  app.post('/api/tags', auth.ensureAuthenticated, auth.isAdmin, tagController.create);
  app.put('/api/tags/:tagId', auth.ensureAuthenticated, auth.isAdmin, tagController.update);
  app.delete('/api/tags/:tagId', auth.ensureAuthenticated, auth.isAdmin, tagController.destroy);

  app.param('tagId', tagController.tag);

  //Inn routes
  var innController = require('../controllers/innController');
  app.get('/api/inns', auth.ensureAuthenticated, innController.all);
  app.get('/api/inns/:innId', innController.show);
  app.post('/api/inns', auth.ensureAuthenticated, auth.isAdmin, innController.create);
  app.put('/api/inns/:innId', auth.ensureAuthenticated, auth.isAdmin, innController.update);
  app.delete('/api/inns/:innId', auth.ensureAuthenticated, auth.isAdmin, innController.destroy);

  app.param('innId', innController.inn);

  //Group routes
  var groupController = require('../controllers/groupController');
  app.get('/api/groups', groupController.all);
  app.post('/api/groups', auth.ensureAuthenticated, auth.isAdmin, groupController.create);
  app.put('/api/groups/:groupId', auth.ensureAuthenticated, auth.isAdmin, groupController.update);
  app.delete('/api/groups/:groupId', auth.ensureAuthenticated, auth.isAdmin, groupController.destroy);

  app.param('groupId', groupController.group);

  //Aided route
  var aidedController = require('../controllers/aidedController');
  app.get('/api/aided', auth.ensureAuthenticated, auth.isPosadero, aidedController.all);              //Listado de asistidos
  app.post('/api/aided', auth.ensureAuthenticated, auth.isPosadero, aidedController.create);          //Crear Asistido
  app.put('/api/aided/:aidedId', auth.ensureAuthenticated, auth.isPosadero, aidedController.update);  //Update asistidos
  app.get('/api/aided/:aidedId', auth.ensureAuthenticated, auth.isPosadero, aidedController.show);    //Obtener un asistido

  //Setting up the aidedId param
  app.param('aidedId', aidedController.aided);

  //Refer Route
  var referController = require('../controllers/referController');
  app.get('/api/refer', auth.ensureAuthenticated, auth.isPosadero, referController.mine);
  app.post('/api/refer', auth.ensureAuthenticated, auth.isVoluntario, referController.create);
  app.post('/auth/refer/', auth.ensureAuthenticated, auth.isPosadero, userController.authUser, referController.refer);

  //Conversation Route
  var conversationController = require('../controllers/conversationController');
  app.get('/api/conversations', auth.ensureAuthenticated, conversationController.mine);
  app.post('/api/conversations', auth.ensureAuthenticated, conversationController.create);
  app.get('/api/conversations/:conversationId', auth.ensureAuthenticated, conversationController.show);
  app.put('/api/conversations/:conversationId', auth.ensureAuthenticated, conversationController.send);

  //Setting up the aidedId param
  app.param('conversationId', conversationController.conversation);

  //Image Routes
  var uploadController = require('../controllers/uploadController');
  app.post('/api/aidedPicture', auth.ensureAuthenticated, aidedController.updatePicture);
  app.post('/api/userPicture', auth.ensureAuthenticated, userController.updatePicture);
  app.post('/api/upload', auth.ensureAuthenticated, uploadController.upload);
  app.get('/uploads/*', auth.ensureAuthenticated, function (req, res) {
    res.sendFile(path.join(__dirname, '../', req.url), function (err) {
      if (err) {
        console.log(err);
        res.status(err.status).end();
      }
    });
  });
  app.get('/static/*', function (req, res) {
    res.sendFile(path.join(__dirname, '../', req.url), function (err) {
      if (err) {
        console.log(err);
        res.status(err.status).end();
      }
    });
  });

  // Session Routes
  var sessionController = require('../controllers/sessionController');
  app.get('/auth/session', auth.ensureAuthenticated, sessionController.session);
  app.post('/auth/session', sessionController.login);
  app.delete('/auth/session', auth.ensureAuthenticated, sessionController.logout);

  // Blog Routes
  var blogs = require('../controllers/blogs');
  app.get('/api/blogs', blogs.all);
  app.post('/api/blogs', auth.ensureAuthenticated, blogs.create);
  app.get('/api/blogs/:blogId', blogs.show);
  app.put('/api/blogs/:blogId', auth.ensureAuthenticated, auth.blog.hasAuthorization, blogs.update);
  app.delete('/api/blogs/:blogId', auth.ensureAuthenticated, auth.blog.hasAuthorization, blogs.destroy);

  //Setting up the blogId param
  app.param('blogId', blogs.blog);

  // Angular Routes
  app.get('/partials/*', function (req, res) {
    var requestedView = path.join('./', req.url);
    res.render(requestedView);
  });

  app.get('/*', function (req, res) {
    if (req.user) {
      res.cookie('user', JSON.stringify(req.user.user_info));
    }

    res.render('index.html');
  });

}
