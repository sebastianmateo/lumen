'use strict';

var constants = require('../controllers/constants.js');
/**
 *  Route middleware to ensure user is authenticated.
 */
exports.ensureAuthenticated = function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.status(401).end();
};

/**
 * Blog authorizations routing middleware
 */
exports.blog = {
  hasAuthorization: function(req, res, next) {
    if (req.blog.creator._id.toString() !== req.user._id.toString()) {
      return res.status(403).end();
    }
    next();
  }
};

//Aided authorization routing middleware
exports.aided = {
    hasAuthorization: function(req, res, next) {
        //Me fijo que la posada del asistido sea la misma que la del usuario
        if (req.aided.inn.toString() != req.user.inn.toString()){
            return res.status(403).end();
        }
        next();
    }
};

exports.isAdmin = function isAdmin(req, res, next){
    if (req.user.role != constants.Role.ADMIN){
        return res.status(403).end();
    }
    next();
};

exports.isPosadero = function isPosadero(req, res, next){
    if (req.user.role != constants.Role.POSADERO){
        return res.status(403).end();
    }
    next();
};

exports.isVoluntario = function isVoluntario(req, res, next){
    if (req.user.role != constants.Role.VOLUNTARIO){
        return res.status(403).end();
    }
    next();
};
