'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema

var ReferSchema = new Schema({
    referReasons: [String],
    necessities: String,
    //A refer can be against a Group or against a User
    group: {
        type:Schema.ObjectId,
        ref: 'Group'
    },
    targetUser: {
      type:Schema.ObjectId,
      ref: 'User'
    },
    aided: {
        type: Schema.ObjectId,
        ref: 'Aided'
    },
    sourceUser: {
      type: Schema.ObjectId,
      ref: "User"
    },
    date: {
      type: Date,
      required: true
    }
});

ReferSchema.statics = {
	load: function(id, cb) {
		this.findOne({
			_id: id
		}).exec(cb);
	}
};

mongoose.model('Refer', ReferSchema);
