'use strict';

var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var GroupSchema = new Schema({
  name: String,
  day: String,
  time: String,
  inn: {
    type: Schema.ObjectId,
    ref: 'Inn'
  }
});

GroupSchema.statics = {
  load: function(id, cb) {
    this.findOne({
      _id: id
    })
      .exec(cb);
  }
};

mongoose.model('Group', GroupSchema);
