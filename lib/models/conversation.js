'use strict';

var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var ConversationSchema = new Schema({
    participants: [{
        type: Schema.ObjectId,
        ref: 'User'
    }],
    sent: Date,
    lastSent: Date,
    subject: String,
    messages: [{
        from: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        content: String,
        sent: Date,
        read: Boolean
    }]
});

ConversationSchema.statics = {
    load: function(id, cb) {
        this.findOne({
            _id: id
        }).populate('participants', '_id name lastName')
          .exec(cb);
    }
};

mongoose.model('Conversation', ConversationSchema);
