'use strict';

var mongoose = require('mongoose'),
Schema = mongoose.Schema,
Group = mongoose.model('Group');

var InnSchema = new Schema({
  name: String,
  phone: String,
  address: String,
  groups: [{
    type: Schema.ObjectId,
    ref: 'Group'
  }]
});

InnSchema.statics = {
  load: function(id, cb) {
    this.findOne({
      _id: id
    })
      .populate('groups')
      .exec(cb);
  }
};

mongoose.model('Inn', InnSchema);
