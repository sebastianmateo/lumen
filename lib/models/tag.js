'use strict';

var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var TagSchema = new Schema({
  name: String,
  category: Number,
  subtags: [{
      type: Schema.ObjectId,
      ref: 'Tag'
  }],
  parent: {
    type: Schema.ObjectId,
    ref: 'Tag'
  },
  sections: [String]
});

TagSchema.statics = {
    load: function(id, cb) {
        this.findOne({
            _id: id
        })
          .populate('subtags')
          .exec(cb);
    }
};

mongoose.model('Tag', TagSchema);
