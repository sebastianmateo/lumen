'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
  crypto = require('crypto');

var InviteSchema = new Schema({
	email: {
		type: String,
		required: true
	},
  group: {
    type: Schema.ObjectId,
    required: true,
    ref: 'Group'
  },
	hashedKey: String,
  date: {
    type: Date,
    required: true
  },
  status: Number
});

InviteSchema
  .virtual('key')
  .set(function (key) {
    this._key = key;
    this.hashedKey = crypto.randomBytes(32).toString('hex');
  })
  .get(function () {
    return this._key;
  });

InviteSchema.statics = {
    load: function(id, cb) {
        this.findOne({
            _id: id
        }).exec(cb);
    }
};

mongoose.model('Invite', InviteSchema);
