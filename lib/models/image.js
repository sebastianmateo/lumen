'use strict';

var mongoose = require('mongoose'),
Schema = mongoose.Schema

var ImageSchema = new Schema({
  name: String,
  image_data : Buffer
});

mongoose.model('Image', ImageSchema);