'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var AidedSchema = new Schema({
	name: String,
	lastName: String,
	dni: String,
  users: [{
    type: Schema.ObjectId,
    ref: 'User'
  }],
    group: {
        type:Schema.ObjectId,
        ref: 'Group'
    },
    profilePicture: {
        type: String,
		default: "default.png"
    },
    personal : {
        //El enabled es un hack horrible para que me traiga el objeto cuando está vacío
        enabled: {type: Boolean, default: true},
        gender: Boolean,
        medicalInsurance: Boolean,
        nationality: String,
        dateOfBirth: Date,
        placeOfBirth: String,
        mobilePhoneNumber: String,
        immigrationDate: Date,
        homeless:Boolean,
        places: [{
            city:String,
            province: String,
            street:String,
            streetNumber:Number,
            neighborhood: String,
            timetable: [{
                days: [{
					type: String,
					details: String
				}],
                startHour: String,
                endHour: String
            }]
        }],
        maritalStatus:String,
        livingWith:String,
        city:String,
        province: String,
        street:String,
        streetNumber:Number,
        neighborhood: String,
        residencePhoneNumber: String,
        residenceContactName: String,
        residenceContactPhoneNumber: String,
        residenceContactEmail: String
    },
    family : {
        enabled: {type: Boolean, default: true},
        cohabitants: [{
            relation:String,
            name:String,
            lastName: String
        }],
        motherAlive: Boolean,
        fatherAlive: Boolean,
        motherInContact: Boolean,
        fatherInContact: Boolean,
        motherName: String,
        fatherName: String,
        motherContact: String,
        fatherContact: String,
        hasChildren: Boolean,
        children : [{
            name: String,
            lastName: String,
            age: Number,
            inContact: Boolean,
            contact: String
        }]
    },
    clinical : {
        enabled: {type: Boolean, default: true},
        height: Number,
        weight: Number,
        diseases: [{
			category: String,
            name: String,
            details: String
        }],
        morbidHistory: String,
        obGynRecord: String,
        habits: String,
        familyHistory: String,
        immunizations: String,
        disabilities: [{
            type: String,
            details: String
        }],
        previousMedicalConsultations: [{
            date: Date,
            institution: String,
            specialty: String,
            mdFullName: String,
            diagnosis: String,
            treatment: String,
            medicalPrescriptions: [{
                drug: String,
                dose: Number,
                doseUnit: String,
                dosingInterval: Number,
                treatmentDuration: Number,
                route: String
            }]
        }],
        usualMedication: String,
        knownMedicationAllergiesAndAdverseEffects: String,
        treatmentStatus:String,
        treatmentAbandonmentReasons:String,
        treatmentAbandonmentDate:Date,
        hasGeneralPractitioner:Boolean,
        generalPractitionerName: String,
        generalPractitionerSpecialty: String,
        generalPractitionerPhoneNumber: String,
        generalPractitionerEmail: String,
        mentalHealthDiagnosis: String
    },
    mental : {
        enabled: {type: Boolean, default: true},
        mentalState: String,
        observableSymptoms: [{
            type: String,
            details: String
        }],
        disorders: [{
            name: String,
            details: String,
        }],
        conditionType: String,
        oriented: Boolean,
        signsOfIntoxication: Boolean,
        speechType: String,
        selfMedicated: Boolean,
        medications: [{
            drug: String,
            dose: Number,
            doseUnit: String,
            dosingInterval: Number,
            route: String,
            from: Date,
            to: Date,
            currentlyTaking: Boolean
        }],
        treatments: [{
            category: String,
            from: Date,
            to: Date,
            inProgress:Boolean,
            psychiatristName: String,
            mentalDiagnosis: String,
            status: String,
            abandonmentReasons: String,
            medicalPrescriptions: [{
                issuerName: String,
                drug: String,
                dose: Number,
                doseUnit: String,
                dosingInterval: Number,
                route: String,
                from: Date,
                to: Date,
                currentlyTaking: Boolean
            }],
            institutionName: String,
            institutionAddress: String,
            institutionPhoneNumber: String,
            institutionEmail: String,
            doctorName: String,
            doctorPhoneNumber: String,
            doctorEmail: String,
            professionals: [{
                name: String,
                profession: String,
                position: String
            }]
        }],
        episodesOfAggression: [{
            target: String,
            category: String,
            place: String,
            date: Date
        }],
        needsDerivation: String,
        needsInstitutionalization: String,
        institutionName: String,
        institutionAddress: String,
        institutionPhoneNumber: String,
        institutionEmail: String,
        institutionPointOfContact: String,
        mentalHealthDiagnosis: String
    },
    addictions : {
        enabled: {type: Boolean, default: true},
        ageOfOnset: Number,
        startSubstance: String,
        drugs: [{
            name: String,
            frequency: String,
            route: String,
            consuming: Boolean
        }],
        consuming: Boolean,
        timeClean: String,
        hasDualPathology: String,
        dualPathologyType: String,
        treatments: [{
            category: String,
            from: Date,
            to: Date,
            inProgress:Boolean,
            institutionName: String,
            institutionAddress: String,
            institutionPhoneNumber: String,
            institutionEmail: String,
            doctorName: String,
            doctorPhoneNumber: String,
            doctorEmail: String
        }],
        episodesOfAggression: [{
            target: String,
            category: String,
            place: String,
            date: Date
        }],
        needsDerivation: String,
        needsInstitutionalization: String,
        institutionName: String,
        institutionAddress: String,
        institutionPhoneNumber: String,
        institutionEmail: String,
        institutionPointOfContact: String,
        pregnant: Boolean,
        observations: String
    },
    socialServices : {
        enabled: {type: Boolean, default: true},
        services: [{
            category: String,
            from: Date,
            to: Date,
            currentlyUsing: Boolean,
            provider:String,
            address: String,
            phoneNumber: String,
            email: String,
            pointOfContact: String
        }]
    },
    education : {
        enabled: {type: Boolean, default: true},
        elementaryStatus:String,
        highSchoolStatus: String,
        communityCollegeStatus: String,
        collegeStatus: String,
        takingTrainingCourses: Boolean,
        elementarySchools:[{
            schoolName: String,
            address: String,
            from: Date,
            to: Date,
            title: String,
            referrers:[{
                name: String,
                lastName: String,
                position: String,
                phoneNumber: String,
                email: String
            }]
        }],
        highSchools:[{
            schoolName: String,
            address: String,
            from: Date,
            to: Date,
            title: String,
            referrers:[{
                name: String,
                lastName: String,
                position: String,
                phoneNumber: String,
                email: String
            }]
        }],
        communityColleges:[{
            schoolName: String,
            address: String,
            from: Date,
            to: Date,
            title: String,
            referrers:[{
                name: String,
                lastName: String,
                position: String,
                phoneNumber: String,
                email: String
            }]
        }],
        colleges:[{
            schoolName: String,
            address: String,
            from: Date,
            to: Date,
            title: String,
            referrers:[{
                name: String,
                lastName: String,
                position: String,
                phoneNumber: String,
                email: String
            }]
        }],
        courses : [{
            name:String,
            institutionName:String,
            academicArea: String,
            address: String,
            from: Date,
            to: Date,
            state: String,
            hasGrant: Boolean,
            grantAmount: Number,
            title: String,
            materials: Boolean,
            laborOutput: Boolean,
            working: Boolean
        }]
    },
    job : {
        enabled: {type: Boolean, default: true},
        trades: [{
            name:String
        }],
        working: Boolean,
        jobs: [{
            employerName: String,
            address: String,
            position: String,
            description: String,
            from: Date,
            to: Date,
            references: [{
                name: String,
                lastName: String,
                independent: Boolean,
                position: String,
                phoneNumber: String,
                email: String
            }]
        }]
    },
    legal : {
        enabled: {type: Boolean, default: true},
        hasPoliceRecord: String,
        branchOfLaw: String,
        policeRecordDetails: String,
        trialDetails: String,
        represented: Boolean,
        lawyerDetails: String,
        status: String,
        recommendation: String
    },
    donations : {
        enabled: {type: Boolean, default: true},
        category: [{type: String}],
        observations: String
    },
    innkeeper : {
        enabled: {type: Boolean, default: true},
        biopsychosocialDiagnosis: String,
        plan: [{
            goal: String,
            from: Date,
            to: Date,
            action: String
        }]
    }
});

AidedSchema.statics = {
	load: function(id, sections, cb) {
        console.log("Secciones: " + sections.join(" "));
		this.findOne({_id: id})
        .select('name lastName dni inn profilePicture users ' + sections.join(" "))
        .exec(cb);
	}
};

AidedSchema.set('toJSON', { minimize: true });
AidedSchema.set('toObject', { minimize: true });

mongoose.model('Aided', AidedSchema);
