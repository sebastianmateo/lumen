'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  Invite = mongoose.model('Invite'),
  crypto = require('crypto'),
  constants = require('../controllers/constants.js');

var UserSchema = new Schema({
  email: {
    type: String,
    unique: true,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  province: String,
  city:String,
  institution:String,
  role: {
    type: Number,
    required: true
  },
  tags: [{
    type: Schema.ObjectId,
    ref: 'Tag'
  }],
  group: {
    type: Schema.ObjectId,
    ref: 'Group'
  },
  profilePicture: {
    type: String,
      default: 'default.png'
  },
  events: [{
    category: Number,
    aidedId: String,
    aidedName: String,
    userId: String,
    userName: String,
    changes: Object,
    date: Date,
	read: Boolean
  }],
  aided: [{
    type: Schema.ObjectId,
    ref: 'Aided'
  }],
  hashedPassword: String,
  salt: String,
  admin: Boolean,
  guest: Boolean,
  provider: String
});

/**
 * Virtuals
 */
UserSchema
  .virtual('password')
  .set(function (password) {
    this._password = password;
    this.salt = this.makeSalt();
    this.hashedPassword = this.encryptPassword(password);
  })
  .get(function () {
    return this._password;
  });

UserSchema
  .virtual('user_info')
  .get(function () {
    return {
      '_id': this._id,
      'name': this.name,
      'email': this.email,
      'role': this.role,
      'aided': this.aided,
      'profilePicture': this.profilePicture
    };
  });

/**
 * Validations
 */

var validatePresenceOf = function (value) {
  return value && value.length;
};
/*
 UserSchema.path('email').validate(function (value, respond) {
 mongoose.models["User"].findOne({email: value}, function (err, user) {
 if (err) throw err;
 if (user) return respond(false);
 respond(true);
 });
 }, 'The specified email address is already in use.');
 */
/**
 * Pre-save hook
 */

UserSchema.pre('save', function (next) {
  //If we are updating, continue
  if (!this.isNew) {
    return next();
  }

  //Validate password
  if (!validatePresenceOf(this.password)) {
    next(new Error('Invalid password'));
  } else {
    //Update Invitation Status
    if (this.role == constants.Role.POSADERO) {
      Invite.findOneAndUpdate(
        {"email": this.email},
        {"status": constants.InviteStatus.ACCEPTED},
        function (err) {
          if (err) {
            next(new Error('Internal Error'));
          } else {
            next();
          }
        }
      )
    } else {
      next();
    }
  }
});

/**
 * Methods
 */

UserSchema.methods = {

  /**
   * Authenticate - check if the passwords are the same
   */

  authenticate: function (plainText) {
    return this.encryptPassword(plainText) === this.hashedPassword;
  },

  /**
   * Make salt
   */

  makeSalt: function () {
    return crypto.randomBytes(16).toString('base64');
  },

  /**
   * Encrypt password
   */

  encryptPassword: function (password) {
    if (!password || !this.salt) return '';
    var salt = new Buffer(this.salt, 'base64');
    return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
  }
};

UserSchema.statics = {
  load: function(id, cb) {
    this.findOne({
      _id: id
    })
      .exec(cb);
  },
  profile: function(id, cb) {
    this.findOne({
      _id: id
    })
      .select('email name lastName province city institution role tags group profilePicture')
      .populate('tags')
      .populate('group')
      .lean()
      .exec(cb);
  }
};

mongoose.model('User', UserSchema);
